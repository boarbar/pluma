

import os
import importlib
import inspect


class Functions:
    def __init__(self):
        files = os.listdir('user')
        for name in files:
            if name.startswith('__'):
                continue
            name = name.replace('.py', '')
            path = 'user.' + name
            module = importlib.import_module(path)
            for name, value in inspect.getmembers(module):
                if not inspect.isfunction(value):
                    continue
                setattr(self, name, value)

    def register(self, function):
        name = function.__name__
        setattr(self, name, function)

functions = Functions()
