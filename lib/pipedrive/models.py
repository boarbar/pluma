

class Model:
    PATH = ''
    ATTRIBUTES = {}
    ALIASES = {}

    def __init__(self, **kwargs):
        for key, value in list(kwargs.items()):
            try:
                alias = self.ALIASES[key]
            except KeyError:
                continue
            else:
                kwargs[alias] = value
        for attr, default in self.ATTRIBUTES.items():
            value = kwargs.get(attr, default)
            setattr(self, attr, value)

    def __repr__(self):
        name = type(self).__name__
        attributes = self.ATTRIBUTES.keys()
        attributes = [
            '{}={}'.format(attr, getattr(self, attr)) for attr in attributes
        ]
        values = '; '.join(attributes)
        return '<{name}({values})>'.format(name=name, values=values)


class Organization(Model):
    PATH = 'organizations'
    ATTRIBUTES = {
        'id': None,
        'address': '',
        'address_admin_area_level_1': '',
        'address_country': '',
        'address_formatted_address': '',
        'address_locality': '',
        'address_postal_code': '',
        'address_route': '',
        'address_street_number': '',
        'contract_status': '',
        'final_startdate': '',
        'last_activity_date': '',
        'logsys_customer_id': '',
        'name': '',
        'notified_opening_date': '',
        'opening_by_eo': '',
        'owner_name': '',
        'process': '',
        'ramp_up_quartal': '',
        'rsm': '',
        'sap': '',
        'spare_part_equipping': '',
        'start_eo': '',
        'status_rampup': '',
        'storage_position': '',
        'technician_in_market': '',
    }
    ALIASES = {
        '0d587bab11dce9063eb412b17ea7eeb454d83f42': 'status_rampup',
        '3180749eec96f90bc541dda271a283f9291d80b0': 'ramp_up_quartal',
        '327291817465ba9bcffa9b2cac8135d40324d4fd': 'opening_by_eo',
        '3bb0c664043ec211984a8a730b5d693fce444de4': 'rsm',
        '437ff10112d13d4e3d91faddf7f0cc082bc036dd': 'technician_in_market',
        '4c3ae65995e5d7ba55100c55f7f0c3e651c24162': 'storage_position',
        '5095915c05413581b293f4c1696d756650dfc8a3': 'spare_part_equipping',
        '607abbd60d820aefc4b9170f019b1fc786c6110c': 'final_startdate',
        '72e0937dafbeb2fddd9e4f4612c049bcb87c16d7': 'logsys_customer_id',
        '8aa16fcaa35e184623f9d9b10012d1f540675af2': 'process',
        '93d0946fc7b01c6bd9341c259250af196d71b249': 'sap',
        'da288c08b81cd5a4b0884f5f63c3185a94b90435': 'start_eo',
        'df3bdd35edcfc194e1364ad783755509e3d420ef': 'notified_opening_date',
        'fd9032c87cf088b116a293f5a9bd510ee940a980': 'contract_status',
    }


class OrganizationField(Model):
    PATH = 'organizationFields'
    ATTRIBUTES = {
        'id': None,
        'key': '',
        'name': '',
        'options': [],
    }


class Filter(Model):
    PATH = 'filters'
    ATTRIBUTES = {
        'id': None,
        'name': '',
        'type': '',
    }


class Deal(Model):
    PATH = 'deals'
    ATTRIBUTES = {
        'id': None,
    }


class User(Model):
    PATH = 'users'
    ATTRIBUTES = {
        'id': None,
    }


class Activity(Model):
    PATH = 'activitiies'
    ATTRIBUTES = {
        'id': None,
        'company_id': '',
        'user_id': '',
        'done': '',
        'type': '',
        'due_date': '',
        'add_time': '',
        'marked_as_done_time': '',
        'subject': '',
        'deal_id': '',
        'org_id': '',
        'person_id': '',
        'person_name': '',
        'org_name': '',
        'deal_title': '',
        'owner_name': '',
    }
