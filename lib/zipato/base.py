

from hashlib import sha1
import requests

from . import models


class API:
    BASE_URI = 'https://my.zipato.com:443//zipato-web/'
    proxies = {}

    def login(self, user, password, proxies={}):
        password = sha1(password.encode('utf-8')).hexdigest()
        self.proxies = proxies
        path = 'rest/user/init'
        try:
            response = self.request('GET', path, proxies=proxies)
        except requests.exceptions.ConnectionError:
            return False
        self.api_token = response['data'][0]['api_token']
        nonce = response.json()['nonce']
        token = sha1((nonce + password).encode('utf-8')).hexdigest()

        # login
        response = self.request(
            'POST',
            '/json/Login',
            data={'username': user, 'password': token, 'method': 'SHA1'},
        )

        response = response.json()
        if response['success']:
            return True
        else:
            return False

    def request(self, method, path, params={}, data={}):
        uri = self.BASE_URI + path

        session = requests.Session()
        methods = {
            'GET': session.get,
            'POST': session.post,
        }
        response = methods[method](
            uri,
            params=params, data=data, proxies=self.proxies,
        )
        resp_json = response.json()
        return resp_json

    def query(self, model):
        return Query(self, model)


class Query:
    def __init__(self, api, Model):
        self.api = api
        self.Model = Model
        self.filters = {}

    def filter_by(self, filters):
        self.filters = filters

    def get(self, params):
        response = self.api.request(
            method='GET',
            path=self.Model.PATH,
            params=params,
        )
        return response

    def get_one(self):
        raise NotImplementedError
        return list(self.get_many('1'))[0]

    def get_many(self, limit, start=0):
        raise NotImplementedError
        response = self.get(params={'limit': limit})

        for data in response['data']:
            model = self.Model(**data)
            yield model

    def get_all(self):
        raise NotImplementedError
        start = 0
        limit = 50
        while True:
            response = self.get(params={'start': start, 'limit': limit})
            pagination = response['additional_data']['pagination']
            for data in response['data']:
                model = self.Model(**data)
                yield model

            if pagination['more_items_in_collection']:
                start += 50
                limit += 50
            else:
                break
