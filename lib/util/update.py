
"""
This module was meant to automatically check for updates.
It will either be replaced by GIT or continued later on.
"""
raise NotImplementedError

import os


def update(update_path):
    def walk(path):
        try:
            directory = os.listdir(path)
        except NotADirectoryError:
            old_dir = path.replace(update_path, '')
            with open(path, 'r') as f:
                content = f.read()

            with open(old_dir, 'r') as f:
                old_content = f.read()

            if content != old_content:
                print('Not same content')
            else:
                print('Same content')
            return

        for item in directory:
            file_path = os.path.join(path, item)
            walk(file_path)

    walk(update_path)
