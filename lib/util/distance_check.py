

import difflib
import re


def old_get_list(word, possibilities, n=3, cutoff=3):
    def get_worth(comp):
        worth = 0
        for i, letter in enumerate(word):
            try:
                if comp[i] == letter:
                    worth += 1
                else:
                    worth += 0.5
            except IndexError:
                worth += 0  # Explicit is better than implicit
        return worth

    results = []
    for possibility in possibilities:
        worth = get_worth(possibility)
        results.append((possibility, worth))

    results = sorted(results, key=lambda item: item[1], reverse=True)
    return [item[0] for item in results]


def get_list2(word, possibilities, n=3, cutoff=0.6):
    u = difflib.SequenceMatcher(a=word)
    l = difflib.SequenceMatcher(a=word.lower())
    results = []
    for psb in possibilities:
        u.set_seq2(psb)
        l.set_seq2(psb.lower())
        uratio = u.ratio()
        lratio = l.ratio()
        ratio = (uratio + lratio) / 2
        print(psb, uratio, lratio)
        if ratio >= cutoff:
            results.append((ratio, psb))
    results = sorted(results, key=lambda item: item[0], reverse=True)
    results = [item[1] for item in results]
    return results[:min(n, len(results))]


def match_ratio(word, possibility, cutoff=0.7):
    assert cutoff >= 0.0

    def recursion(word, ratio):
        if ratio <= cutoff:
            return 0

        if re.match(word, possibility):
            return ratio

        mod = 1.0 / len(word)
        ratio -= mod
        for i, letter in enumerate(word):
            lword = list(word)

            lword[i] = letter.swapcase()
            tword = '.*' + ''.join(lword) + '.*'
            if re.match(tword, possibility):
                return ratio + mod / 2

            lword[i] = '.'
            tword = '.*' + ''.join(lword) + '.*'
            if re.match(tword, possibility):
                return ratio

        for i, letter in enumerate(word):
            lword = list(word)

            lword[i] = letter.swapcase()
            ratio = recursion(''.join(lword), ratio)
            if ratio > 0:
                return ratio + mod / 2

            lword[i] = '.'
            ratio = recursion(''.join(lword), ratio)
            if ratio > 0:
                return ratio

        return 0

    if len(word) > len(possibility):
        return 0
    return recursion(word, 1.0)


def get_list(word, possibilities, n=3, cutoff=0.7):
    results = []
    for psb in possibilities:
        ratio = match_ratio(word, psb, cutoff)
        if ratio >= cutoff:
            results.append((ratio, psb))

    results = sorted(results, key=lambda item: item[0], reverse=True)
    results = sorted(results, key=len, reverse=True)
    results = [item[1] for item in results]
    return results[:min(n, len(results))]


if __name__ == '__main__':
    import random
    word = 'Hello'
    tests = ['hello', 'Hello1', 'ello', 'ohello', 'Bello', 'olleH']
    word = 'nth'
    tests = ['Name']

    random.shuffle(tests)
    result = get_list(word, tests, 100, 0.0)
    print(result)
