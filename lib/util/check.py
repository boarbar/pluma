

from datetime import datetime


def is_float(word):
    try:
        float(word)
    except (ValueError, TypeError):
        return False
    else:
        return True


def is_int(word):
    try:
        int(word)
    except (ValueError, TypeError):
        return False
    else:
        return True


def date_or_none(text, format_='%d.%m.%Y'):
    try:
        datetime.strptime(text, format_)
    except Exception:
        return None
    else:
        return text
