

"""RegEx related util functions"""


def date_to_regex(start, end):
    raise NotImplementedError()
    form = '{year}-{month}-{day}'
    start = form.format(year=start.year, month=start.month, day=start.day)
    end = form.format(year=end.year, month=end.month, day=end.day)
    regex = ''
    aligned = True
    for i in range(len(start)):
        if aligned and start[i] == end[i]:
            regex += start[i]
        else:
            aligned = False


if __name__ == '__main__':
    from datetime import datetime

    start = datetime(2015, 12, 5)
    end = datetime(2016, 3, 4)
    date_to_regex(start, end)
