

def fill(word, filling=' ', mod_after=10):
    to_fill = mod_after - len(word.replace('{f}', '')) % mod_after
    filling = filling * to_fill
    if '{f}' in word:
        result = word.format(f=filling)
    else:
        result = word + filling
    return result


if __name__ == '__main__':
    print('>', fill('hello', '^'), '<')
    print('>', fill('hello there', '^'), '<')
    print('>', fill('hello there you{f}', '^'), '<')
    print('>', fill('hello there you1{f}', '^'), '<')
    print('>', fill('hello there you12{f}', '^'), '<')
    print('>', fill('hello there you123{f}', '^'), '<')
    print('>', fill('hello there you1234{f}', '^'), '<')
    print('>', fill('hello there you12345{f}', '^'), '<')
    print('>', fill('hello there you123456{f}', '^'), '<')
    print('>', fill('hello there you1234567{f}', '^'), '<')
    print('>', fill('hello there you12345678{f}', '^'), '<')
    print('>', fill('hello there you123456789{f}', '^'), '<')
    print('>', fill('hello there you1234567890{f}', '^'), '<')
