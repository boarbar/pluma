

import requests


class Session(requests.Session):
    opened = 0

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        Session.opened += 1

    def __del__(self, *args, **kwargs):
        Session.opened -= 1
