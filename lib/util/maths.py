

def letters_to_number(letters):
    letters = letters.upper()
    leng = len(letters)
    number = 0

    for i, l in enumerate(letters, 1):
        n = ord(l) - 64
        number += n * 26 ** (leng - i)

    return number


def number_to_letters(number):
    def recursion(num, letters):
        num, remain = divmod(num, 26)
        letter = chr(remain + 64)
        letters.insert(0, letter)
        if num != 0:
            letters = recursion(num, letters)
        return letters

    number = int(number)
    letters = []
    return ''.join(recursion(number, letters))


def split_numbers_and_letters(cell):
    letters = ''.join([l for l in cell if l.isalpha()])
    numbers = cell.replace(letters, '')
    return numbers, letters


if __name__ == '__main__':
    letters = 'BA'
    result = letters_to_number(letters)
    print(result, ' == ', 53)

    letters = 'BAC'
    result = letters_to_number(letters)
    print(result, ' == ', 1381)

    number = 53
    result = number_to_letters(number)
    print(result, ' == ', 'BA')

    number = 1381
    result = number_to_letters(number)
    print(result, ' == ', 'BAC')
