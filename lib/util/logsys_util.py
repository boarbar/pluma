

import re


def match(pattern, value):
    """Make a LogSys dialect of RegEx match."""
    if pattern is None or value is None:
        return pattern == value
    pattern, value = str(pattern), str(value)
    comp = pattern.split('|')
    for c in comp:
        if c == '':
            continue
        if c[0] == '!':
            if not re.match(c[1:], value, re.I):
                return True
        if c[0] == '>':
            try:
                if int(value) > int(c[1:]):
                    return True
            except ValueError:
                if str(value) > str(c[1:]):
                    return True
        if c[0] == '<':
            try:
                if int(value) < int(c[1:]):
                    return True
            except ValueError:
                if str(value) < str(c[1:]):
                    return True
        if c.count('~') == 1:
            start, end = c.split('~')
            try:
                if int(value) in range(int(start), int(end)+1):
                    return True
            except ValueError:
                pass
        if re.match('^{}$'.format(pattern).replace('*', '.*'), value, re.I):
            return True
    return False


if __name__ == '__main__':
    print(match('20(1[0-5]*|16-0([12]*|3-(0[1-9]*|10*)))', '2015-06-05eoaue'))
