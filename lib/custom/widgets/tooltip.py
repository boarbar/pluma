

import tkinter as tk


class ToolTip:
    def __init__(self, widget, text, enabled=True):
        self.widget = widget
        self.enabled = enabled
        self.text = text.replace('\\n', '\n').replace('\\t', '\t')
        self.tw = None
        self.widget.bind('<Enter>', self.enter)
        self.widget.bind('<Leave>', self.close)

    def enter(self, event=None):
        if not self.enabled:
            return
        x = y = 0
        x, y, cx, cy = self.widget.bbox('insert')
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        self.tw = tk.Toplevel(self.widget)
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry('+%d+%d' % (x, y))
        label = tk.Label(self.tw, text=self.text, justify='left',
                         background='#F7F199', relief='solid',
                         borderwidth=1, font=('times', '8', 'normal'))
        label.pack(ipadx=1)

    def close(self, event=None):
        if self.tw:
            self.tw.destroy()

    def enable(self):
        self.enabled = True

    def disable(self):
        self.enabled = False
