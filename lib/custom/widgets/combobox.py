

import difflib

from tkinter import StringVar
from tkinter.ttk import Combobox

from common.localise import loc


class ComboBox(Combobox):
    def __init__(self, parent, export=[], *args, **kwargs):
        try:
            self.var = kwargs['textvariable']
        except KeyError:
            self.var = StringVar()
            kwargs['textvariable'] = self.var
        try:
            self._values = kwargs['values']
        except KeyError:
            self._values = []
        self.export = export
        super().__init__(parent, *args, **kwargs)
        self['values'] = self._values[:]

    def values(self, values=None, export=[]):
        """Set values and export if given and return values."""
        if values is not None:
            values = list(values)
            self._values = values
            self['values'] = values[:]
            self.export = export
        return self._values[:]

    def get_value(self):
        """Return closest export value to currently selected value."""
        value = self.closest_value(self.var.get())
        return value

    def closest_value(self, value):
        value = value.lower()
        lmapping = [loc(m).lower() for m in self.export]
        value = difflib.get_close_matches(value.lower(),
                                          lmapping, 1, 0)[0]
        return self.export[lmapping.index(value)]
