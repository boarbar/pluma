

from collections import OrderedDict
import tkinter
from tkinter import ttk


class ItemsListing(ttk.Frame):
    def __init__(self, root, columns=3):
        ttk.Frame.__init__(self, root)
        self.items = OrderedDict()
        self.pinned = []
        self.disabled = {}
        self.columns = columns

    def _regrid(self):
        for i, item in enumerate(self.items.values()):
            row, column = divmod(i, self.columns)
            item.grid(
                row=row,
                column=column,
                sticky='nwe',
            )

    def add(self, key, text):
        sv = tkinter.StringVar(value=text)
        item = ttk.Label(self, textvariable=sv, relief='raised', padding=1)
        item.sv = sv
        item.bind('<Button-1>', lambda ev: self.pop(key))
        item.bind('<Button-2>', lambda ev: self.pin(key))
        item.bind('<Button-3>', lambda ev: self.toggle(key))

        try:
            self.items[key].grid_forget()
        except KeyError:
            pass
        self.items[key] = item
        self._regrid()

    def pop(self, key):
        if key in self.disabled:
            return
        item = self.items.pop(key)
        item.grid_forget()
        if key in self.pinned:
            self.pinned.remove(key)
        self.on_pop(key)
        self._regrid()

    def pin(self, key):
        item = self.items[key]
        text = item.sv.get()
        if key in self.pinned:
            self.pinned.remove(key)
            item.sv.set(text[2:-2])
        else:
            self.pinned.append(key)
            item.sv.set('► {} ◄'.format(text))

    def toggle(self, key):
        if key in self.disabled.keys():
            item = self.disabled.pop(key)
            self.items[key] = item
            state = 'normal'
        else:
            item = self.items.pop(key)
            self.disabled[key] = item
            state = 'disabled'
        item.configure(state=state)
        self.on_toggle()

    def clear(self):
        """Clear all items that are not pinned."""
        keys = list(self.items.keys())
        for key in keys:
            if key in self.pinned:
                continue
            item = self.items.pop(key)
            item.grid_forget()
        self.on_clear()
        self._regrid()

    def on_pop(self, key):
        return

    def on_toggle(self):
        return

    def on_clear(self):
        return
