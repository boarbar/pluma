

from types import SimpleNamespace

from tkinter import Label

from .scroll_frame import ScrollFrame


class Table(ScrollFrame):
    def __init__(self, parent, enable_header=True, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.columns = []
        self.headers = []
        self.rows = []
        self.header_enabled = enable_header

    def toggle_header(self, status=None):
        if status is None:
            status = not self.header_enabled
        elif status:
            self.header_enabled = True
            for i, header in enumerate(self.headers):
                header.grid(row=0, column=i)
        else:
            self.header_enabled = False
            for header in self.headers:
                header.grid_forget()

    def add_column(self, widget, header='', **kwargs):
        """Add column info

        Adding a widget class and the kwargs on its creation.

        Arguments:
            widget object -- widget class to use in this column
            **kwargs dict -- kwargs on widget creation
        """
        self.columns.append(
            {
                'widget': widget,
                'header': header,
                'options': kwargs,
                'bindings': {},
            }
        )
        l = Label(self.interior, text=header)
        if self.header_enabled:
            l.grid(row=0, column=len(self.headers))
        self.headers.append(l)
        self.toggle_header(self.header_enabled)

    def add_row(self):
        # row = Frame(self.interior)
        # row.pack()
        row = SimpleNamespace(cells=[])
        for i, column in enumerate(self.columns):
            w = column['widget'](self.interior, **column['options'])
            for sequence, function in column['bindings'].items():
                w.bind(sequence, function)
            w.grid(row=len(self.rows) + 1, column=i)
            row.cells.append(w)
        self.rows.append(row)

    def remove_row(self, i):
        row = self.rows.pop(i)
        for cell in row.cells:
            cell.grid_forget()

    def bind_column(self, i, sequence, function):
        for row in self.rows:
            widget = row.cells[i]
            widget.bind(sequence, function)
        self.columns[i]['bindings']['sequence'] = function

    def config_column(self, i, **kwargs):
        for row in self.rows:
            widget = row.cells[i]
            widget.config(**kwargs)
        self.columns[i]['options'].update(kwargs)


if __name__ == '__main__':
    import tkinter as tk

    root = tk.Tk()
    table = Table(root)
    table.grid()
    table.add_column(Label, 'Hello')
    table.add_column(tk.Entry, 'There')
    table.add_row()
    table.add_row()
    root.mainloop()
