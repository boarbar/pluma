

from collections import OrderedDict
from tkinter.ttk import Frame, Label


class Statusbar(Frame):
    def __init__(self, root):
        Frame.__init__(self, root)
        self.items = OrderedDict()

    def _regrid(self):
        for i, item in enumerate(self.items.values()):
            item.grid(
                row=0,
                column=i,
            )

    def add(self, key, text):
        item = Label(self, text=text)

        if key in self.items.keys():
            self.items[key].grid_forget()

        self.items[key] = item
        self._regrid()

    def pop(self, key):
        item = self.items[key]
        item.grid_forget()
        self.items.pop(key)
        self.on_pop(key)
        self._regrid()

    def clear(self):
        """Clear all items."""
        keys = list(self.items.keys())
        for key in keys:
            item = self.items.pop(key)
            item.grid_forget()
        self.on_clear()
        self._regrid()

    def on_pop(self, key):
        return

    def on_clear(self):
        return
