

from tkinter.ttk import Treeview

import settings
from localisation import _


class ChangeTree(Treeview):
    """Specialized Treeview.

    The ChangeTree is specialized to provide a better interface
    for change actions on the Tree.
    """

    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, selectmode='browse', columns=[0, 1],
                         *args, **kwargs)
        self.article_count = 0

        self.column('#0', stretch=False, width=60)
        self.heading(0, text=_('ui.name'))
        self.heading(1, text=_('ui.value'))

        tag_config = settings.get('change_tree_tags', 'base')
        for tag, config in tag_config.items():
            self.tag_configure(
                tag,
                background=config['color'],
            )

    def _dump(self, dossiers, clear, parent):
        if clear:
            self.clear()
        for info in dossiers:
            info.setdefault('value', '')
            info.setdefault('nodes', [])
            info.setdefault('tag', '')
            self.insert(
                parent,
                'end',
                iid=info['key'],
                values=(info['name'], info['value']),
                tag=info['tag'],
            )
            self._dump(info['nodes'], parent=info['key'], clear=False)

    def dump(self, dossiers, clear=True, parent=''):
        """Write dossiers into the tree.

        Args:
            dossiers: multidimensional list of items to write.
        """
        self._dump(dossiers, clear, parent)
        self.select_first()

    def select_first(self):
        children = self.get_children('')
        focus = self.focus()
        self.selection_remove([focus])
        if children:
            self.selection_set((children[0], children[0]))
            self.focus(children[0])

    def get_topmost_focused(self):
        parent = self.focus()
        focus = parent
        while parent != '':
            focus = parent
            parent = self.parent(parent)
        return focus

    def clear(self):
        """Clear the whole tree."""
        children = self.get_children()
        self.delete(*children)
