

from tkinter import Frame, Canvas, Scrollbar


class ScrollFrame(Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.canvas = Canvas(self)
        self.interior = Frame(self.canvas)
        self.vscroll = Scrollbar(
            self, orient='vertical', command=self.canvas.yview
        )

        self.canvas.configure(yscrollcommand=self.vscroll.set)
        self.vscroll.config(command=self.canvas.yview)

        def config_canvas(ev):
            self.canvas.configure(
                scrollregion=self.canvas.bbox('all'), width=200, height=200
            )

        self.vscroll.pack(fill='y', side='right', expand=1)
        self.canvas.pack(side='left', fill='both', expand=1)
        self.interior.pack(side='left', fill='both', expand=1)
        self.interior.bind('<Configure>', config_canvas)

        self.interior_id = self.canvas.create_window(
            0, 0, window=self.interior, anchor='nw',
        )


if __name__ == '__main__':
    import tkinter

    root = tkinter.Tk()
    scroll = ScrollFrame(root)
    one = tkinter.Label(scroll.interior, text='hello')
    two = tkinter.Label(scroll.interior, text='bye')
    one.grid(row=0, column=0)
    two.grid(row=0, column=1)
    scroll.grid()
    root.mainloop()
