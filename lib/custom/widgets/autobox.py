

from tkinter import StringVar
from tkinter.ttk import Combobox

from lib.util import distance_check


class AutoBox(Combobox):
    def __init__(self, master, values=None, textvariable=None, auto_take=True,
                 **kwargs):
        super().__init__(master, **kwargs)
        self.show_lb = False
        if values:
            self._values = values
        else:
            self._values = {}

        if textvariable is None:
            textvariable = StringVar()
        self.var = self['textvariable'] = textvariable
        self.auto_take = auto_take
        self._set_first()
        self._trace_id = self.var.trace('w', self.changed)
        self['postcommand'] = self.changed

    def _set_first(self):
        try:
            text = list(self._values.keys())[0]
        except IndexError:
            text = ''
        self.var.set(text)

    def get(self):
        """Return key of currently selected value."""
        if self.auto_take:
            value = self.var.get()
            if value in list(self._values.keys()):
                key = self._values[value]
            else:
                value = self.comparison(0.0)
                if value:
                    value = value[0]
                    key = self._values[value]
                else:
                    value = ''
                    key = None
                self.var.set(value)
        else:
            value = self.var.get()
            if value in self._values.keys():
                key = self._values[value]
            else:
                key = False
        return key

    def values(self, values=None):
        """Set or get values.

        Args:
            values: dictionary of keys as display value and
                values as export values
        """
        if values is None:
            return self._values
        self._values = values
        self.var.trace_vdelete('w', self._trace_id)
        self._set_first()
        self._trace_id = self.var.trace('w', self.changed)

    def changed(self, *args):
        self['values'] = self.comparison()

    def comparison(self, cutoff=0.7):
        """Return closest values to typed in value."""
        value = self.var.get()
        values = list(self._values.keys())
        if not value or value in values:
            results = values
        else:
            results = distance_check.get_list(value, values, 7, cutoff)
        return results
