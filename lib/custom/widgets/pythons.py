

import random
from operator import add
from tkinter import Canvas


class Pythons(Canvas):
    def __init__(self, root, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.root = root

        self.run = False
        self.width = 100
        self.height = 100

        self.scale = 3
        self.move_speed = 100
        self.spam_rate = 5000
        self.score = 0

    def turn(self, rx, ry):
        same_direction = (rx, ry) == self.last_direction
        opposite_x = rx == -self.last_direction[0]
        opposite_y = ry == -self.last_direction[1]
        if same_direction or opposite_x or opposite_y:
            return
        self.direction = (rx, ry)

    def move(self):
        if not self.run:
            return
        new = tuple(map(
            add,
            self.python[0],
            self.direction
        ))
        if new in self.python:
            self.run = False
        elif self.out_of_area(new):
            self.run = False
            return
        if new in self.spam.keys():
            iid = self.spam[new]
            self.delete(iid)
            self.score += 1
            self.itemconfigure(self.score_display, text=self.score)
        else:
            self.python.pop()
        self.delete(self.python_i)
        self.python.insert(0, new)
        python = [item*self.scale for sublist in self.python
                  for item in sublist]
        self.python_i = self.create_line(
            *python,
            width=self.scale,
            fill='#F0DB26'
        )
        self.move_id = self.root.after(self.move_speed, self.move)
        self.last_direction = self.direction

    def spawn_spam(self):
        if not self.run:
            return
        x = random.randint(1, self.width)
        y = random.randint(1, self.height)
        if (x, y) in self.spam.keys():
            self.spawn_spam()
            return
        nx, ny = x*self.scale, y*self.scale
        iid = self.create_rectangle(nx, ny, nx+self.scale, ny+self.scale,
                                    fill='#F28FAB', width=0)
        self.spam[(x, y)] = iid
        self.spawn_id = self.root.after(self.spam_rate, self.spawn_spam)

    def out_of_area(self, coord):
        x, y = coord
        return x < 0 or x > self.width or y < 0 or y > self.height

    def grid(self, *args, **kwargs):
        super().grid(*args, **kwargs)
        self.run = True

        self.python = [(40, 40), (39, 40)]
        self.direction = (1, 0)
        self.last_direction = self.direction
        self.python_i = None

        self.spam = {}

        self.move_id = self.root.after(self.move_speed, self.move)
        self.spawn_id = self.root.after(self.spam_rate, self.spawn_spam)

        self.iup = self.root.bind_all('<Up>',
                                      lambda ev: self.turn(0, -1), '+')
        self.ido = self.root.bind_all('<Down>',
                                      lambda ev: self.turn(0, 1), '+')
        self.ile = self.root.bind_all('<Left>',
                                      lambda ev: self.turn(-1, 0), '+')
        self.iri = self.root.bind_all('<Right>',
                                      lambda ev: self.turn(1, 0), '+')

        self.create_rectangle(
            0, 0, (self.width+1)*self.scale, (self.height+1)*self.scale
        )
        self.score_display = self.create_text(
            self.width*self.scale+15, 15,
            text=self.score
        )

    def grid_forget(self, *args, **kwargs):
        super().grid_forget(*args, **kwargs)
        self.run = False
        self.root.unbind('<Up>', self.iup)
        self.root.unbind('<Down>', self.ido)
        self.root.unbind('<Left>', self.ile)
        self.root.unbind('<Right>', self.iri)
        self.after_cancel(self.move_id)
        self.after_cancel(self.spawn_id)
        self.delete('all')
