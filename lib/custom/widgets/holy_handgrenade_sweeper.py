

import random
from tkinter import Frame, Button, Label


class HolyHandgrenadeSweeper(Frame):
    def __init__(self, root, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.root = root

        self.run = False

        self.width = 25
        self.height = 10
        self.scale = 20
        self.colors = [
            '#1F148D',  # 1 - Dark Blue
            '#D40B0B',  # 2 - Red
            '#409D24',  # 3 - Green
            '#D8CE33',  # 4 - Yellow
            '#993BAA',  # 5 - Purple
            '#E3800B',  # 6 - Orange
            '#37D38B',  # 7 - Dark Torquois
            '#646464',  # 8 - Dark Gray
        ]

        self.fields = {}
        self.handgrenades = {}

    def create_field(self, x, y, command):
        def toggle_state(ev):
            if ev.widget.state == 'unmarked':
                ev.widget.config(text='\u2691')
                ev.widget.state = 'marked'
            elif ev.widget.state == 'marked':
                ev.widget.config(text='')
                ev.widget.state = 'unmarked'
            elif ev.widget.state == 'uncovered':
                pass
            else:
                raise ValueError(
                    'Field state {} does not exist'.format(ev.widget.state)
                )

        frame = Frame(self.f_field, width=self.scale, height=self.scale)
        frame.grid_propagate(False)
        frame.grid(row=y, column=x)
        frame.columnconfigure(0, weight=1)
        frame.button = Button(frame, command=command)
        frame.button.grid(sticky='nswe', padx=1, pady=1)
        frame.button.state = 'unmarked'
        frame.button.bind('<Button-3>', toggle_state)
        return frame

    def setup(self, difficulty=0.15, seed=None):
        self.run = True
        random.seed(seed)
        field_tally = self.width*self.height
        grenade_count = round(field_tally*difficulty)
        if grenade_count < 0:
            grenade_count = 1
        if grenade_count > field_tally:
            grenade_count = field_tally-1

        self.f_info = Frame(self)
        self.f_info.grid(row=0, column=0)
        self.d_grenades = Label(self.f_info, text=str(grenade_count))
        self.d_grenades.grid(row=0, column=1)
        self.d_fields_left = Label(self.f_info, text='0')
        self.d_fields_left.grid(row=0, column=0)
        self.f_field = Frame(self)
        self.f_field.grid(row=1, column=0)

        fields = [(x, y) for x in range(self.width)
                  for y in range(self.height)]

        for i in range(grenade_count):
            x, y = random.choice(fields)
            fields.remove((x, y))
            grenade = self.create_field(
                x, y,
                command=lambda x=x, y=y: self.explode_grenade((x, y))
            )
            self.handgrenades[(x, y)] = grenade

        self.d_fields_left.config(text=len(fields))
        for x, y in fields:
            field = self.create_field(
                x, y,
                command=lambda x=x, y=y: self.uncover_field((x, y))
            )
            self.fields[(x, y)] = field

    def get_neighbors(self, pos):
        x, y = pos
        fields = [(tx, ty) for tx in range(x-1, x+2)
                  for ty in range(y-1, y+2)]
        fields.remove(pos)
        return fields

    def get_grenade_neighbors(self, pos):
        neighbors = self.get_neighbors(pos)

        for neighbor in neighbors:
            if neighbor in self.handgrenades.keys():
                yield neighbor

    def uncover_field(self, pos):
        field = self.fields[pos]
        if field.button.state != 'unmarked':
            return
        field.button.config(state='disabled', relief='ridge', bg='#A2A2A2')
        field.button.state = 'uncovered'
        tally = len(list(self.get_grenade_neighbors(pos)))

        self.fields.pop(pos)

        if tally == 0:
            for neighbor in self.get_neighbors(pos):
                if neighbor not in self.fields.keys():
                    continue
                else:
                    self.uncover_field(neighbor)
        else:
            field.button.config(
                text=str(tally),
                disabledforeground=self.colors[tally-1],
                font=('Helvetica', '-10', 'bold'),
            )
            self.d_fields_left.config(text=len(self.fields))

    def explode_grenade(self, pos):
        if self.handgrenades[pos].button.state != 'unmarked':
            return
        for field in self.handgrenades.values():
            field.button.config(
                text='\u2600',
                state='disabled',
                font=('Helvetica', '-10', 'bold'),
            )
        for field in self.fields.values():
            field.button.config(state='disabled')

    def grid_forget(self):
        super().grid_forget()
        self.run = False
        self.fields = {}
        self.handgrenades = {}
        for child in self.winfo_children():
            child.destroy()
