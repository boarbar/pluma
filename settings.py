

import os.path
import shutil

import yaml


SETTINGS = {}


def get_object(app=None):
    global SETTINGS
    try:
        return SETTINGS[app]
    except KeyError:
        pass

    with open('default_settings.yml', 'r') as f:
        content = f.read()
    if content:
        settings = yaml.load(content)
    else:
        settings = {}
    path = 'user_settings.yml'
    try:
        with open(path, 'r') as f:
            content = f.read()
    except FileNotFoundError:
        content = None
    if content:
        settings.update(yaml.load(content))

    if app:
        if app == 'base':
            default_path = 'base/default_settings.yml'
            user_path = 'base/user_settings.yml'
        else:
            default_path = 'app/' + app + '/default_settings.yml'
            user_path = 'app/' + app + '/user_settings.yml'
        with open(default_path, 'r') as f:
            content = f.read()
        settings.update(yaml.load(content))
        try:
            with open(user_path, 'r') as f:
                content = f.read()
        except FileNotFoundError:
            with open(user_path, 'w') as f:
                content = None
        if content:
            settings.update(yaml.load(content))

    SETTINGS[app] = settings
    return settings


def get(key, app=None):
    obj = get_object(app)
    value = obj[key]
    if key == 'db_path' and 'template' in value:
        value = check_db(value)
    return value


def write(key, value, app=None):
    if app is None:
        path = 'user_settings.yml'
    else:
        path = 'app/' + app + '/user_settings.yml'

    try:
        with open(path, 'r') as f:
            content = f.read()
    except FileNotFoundError:
        settings = {}
    else:
        if content:
            settings = yaml.load(content)
        else:
            settings = {}

    settings[key] = value

    with open(path, 'w') as f:
        f.write(yaml.dump(settings, default_flow_style=False))


def check_db(source):
    destination = source.replace('template', 'user')
    if not os.path.isfile(destination):
        shutil.copy2(source, destination)
    return destination


if __name__ == '__main__':
    get('db')
