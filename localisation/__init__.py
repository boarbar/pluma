

import logging as log
import yaml

languages = {'BAR': 'BAR.yml', 'DEU': 'DEU.yml', 'ENG': 'ENG.yml'}
language = 'DEU'
table = {}


def set_language(lan):
    log.info('Setting language.')
    global language
    global table
    language = lan
    path = 'localisation/{l}'.format(l=languages[lan])
    try:
        with open(path, mode='r', encoding='utf-8') as f:
            table = yaml.load(f.read())
            if not table:
                table = {}
    except FileNotFoundError:
        log.warning('Language {l} was not  found.'.format(l=lan) +
                    'Using default language instead.')
        return


def _(keys, **kwargs):
    global table
    multiple = True
    if isinstance(keys, str):
        keys = [keys]
        multiple = False

    loc = []
    for key in keys:
        ltable = table.copy()
        path = key.split('.')
        try:
            for k in path:
                ltable = ltable[k]
            assert isinstance(ltable, str)  # Last ltable should be value
        except (KeyError, AssertionError, TypeError):
            log.warning('Could not localise ' + str(key))
            value = key
        else:
            value = ltable.format(**kwargs)
        loc.append(value)
    if not multiple:
        loc = loc[0]
    return loc


set_language(language)
