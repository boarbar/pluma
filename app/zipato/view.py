

import logging as log
from collections import OrderedDict

from localisation import _
from .interface import Zipato
from base.templates import Dossier
from . import store_format
from base.templates.inputs import item


class View(Dossier):
    def __init__(self):
        interface = Zipato()

        super().__init__(interface, 'zipato')
        self.title('ProjectPluma - ' + _('masks.zipato'))

    def _format_dossier(self, dossier):
        data = []
        for store in dossier:
            custom = self.interface('get_custom', store.serial)
            comment = (custom.comment if custom else '')
            sto = store_format.get(store, comment)
            data.append(sto)
        return data

    def search(self):
        super().search()

        children = len(self.f_tree.w_dossier.get_children())
        self.f_status.add('children', _('ui.children') + ' ' + str(children))

    def add_filter(self, field=None, criteria=None, pin=False, refilter=False):
        if field is None:
            field = self.f_search.w_field.get()
        special = (
            'zipabox.online', 'zipabox.need_sync',
            'zipazone.ready', 'zipazone.armed', 'zipazone.tripped',
            'zipazone.bypassed', 'zipazone.sensor_state',
        )
        if field in special:
            if criteria is None:
                criteria = self.f_search.w_criteria.get()
            if criteria == _('zipato.wtrue'):
                criteria = 1
            elif criteria == _('zipato.wfalse'):
                criteria = 0
            self.f_search.w_criteria.var.set(criteria)

        super().add_filter(
            field=field, criteria=criteria, pin=pin, refilter=refilter,
        )

    def synchronize(self):
        def init(data):
            user = data['login.name']
            password = data['login.password']
            success = self.interface('get_session', user, password)
            if success is None:
                self.show_options(
                    title=_('ui.retry_login'),
                    callback=init,
                    items=OrderedDict([
                        item(
                            key='login.name',
                        ),
                        item(
                            key='login.password',
                            show=' ',
                        )]
                    ),
                    done_text=_('ui.ok')
                )
            elif success is False:
                title = _('err.title.network_error')
                msg = _('err.msg.network_error')
                self.show_error(title, msg)
            else:
                self.interface(
                    'synchronize',
                    user, password, on_finish, on_error
                )
                try:
                    self.f_status.pop('current_box_sync')
                except KeyError:
                    pass
                try:
                    self.f_status.pop('last_faulty_box')
                except KeyError:
                    pass
                self.search()

        def on_finish(box_name, current, boxes):
            state = '{current_box}: {box_name} {current}/{total}'.format(
                current_box=_('zipato.current_box'),
                box_name=box_name,
                current=str(current),
                total=str(boxes)
            )
            log.info(state)
            self.f_status.add('current_box_sync', state)
            self.update()

        def on_error(box_name):
            pass
            # print('error called')
            # state = '{faulty_box}: {box_name}'.format(
            #     faulty_box=_('zipato.faulty_box'),
            #     box_name=box_name,
            # )
            # print('error first')
            # self.f_status.add('last_faulty_box', state)
            # self.update()

        self.show_options(
            title=_('ui.login'),
            callback=init,
            items=OrderedDict([
                item(
                    key='login.name',
                ),
                item(
                    key='login.password',
                    show=' '
                )]
            ),
            done_text=_('ui.ok')
        )

    def comment(self):
        id_ = self.get_selected_id()
        if id_ is None:
            return
        serial = ''

        def on_finish(data):
            self.interface('edit_custom', id_, data)

        box = self.interface('get_filtered_dossier', [['zipabox.id', id_]])
        box = box.first()
        serial = box.serial
        custom = self.interface('get_custom', serial)
        comment = (custom.comment if custom else '')

        comment = OrderedDict([
            item(
                key='zipabox.comment',
                value=comment,
            ),
        ])

        self.show_options(
            title=_('ui.add_comment'),
            callback=on_finish,
            items=comment,
        )
        self.search()

    def get_actions(self):
        return OrderedDict([
            ['synchronize', self.synchronize],
            ['comment', self.comment],
        ])
