

import logging as log
from hashlib import sha1
import requests
import threading

from sqlalchemy import func
from sqlalchemy.orm import contains_eager

from base.interface import Interface
import settings
from lib.util import custom_requests
from . import models


class Zipato(Interface):
    def __init__(self):
        super().__init__()
        self.MAP['get_session'] = self.get_session
        self.MAP['get_search_criteria'] = self.get_search_criteria
        self.MAP['get_dossier'] = self.get_dossier
        self.MAP['get_filtered_dossier'] = self.get_filtered_dossier
        self.MAP['get_custom'] = self.get_custom
        self.MAP['edit_custom'] = self.edit_custom
        self.MAP['synchronize'] = self.synchronize

    def get_search_criteria(self):
        return [
            'zipabox.name', 'zipabox.online', 'zipabox.need_sync',
            'zipabox.sync_date', 'zipabox.firmware_version',
            'zipazone.name', 'zipazone.ready', 'zipazone.armed',
            'zipazone.tripped', 'zipazone.bypassed', 'zipazone.sensor_state',
            'zipazone.battery_level', 'zipazone.battery_timestamp',
            'zipacontact.name', 'zipacontact.surname', 'zipacontact.email',
            'zipacontact.phone', 'zipacontact.mobile_phone',
        ]

    def get_dossier(self):
        return self.get_filtered_dossier({})

    def get_filtered_dossier(self, filters):
        session = models.Session()
        q = session.query(models.Zipabox).\
            outerjoin(models.Zipazone).\
            outerjoin(models.Zipacontact, models.Zipabox.contacts).\
            options(
                contains_eager(models.Zipabox.zones),
                contains_eager(models.Zipabox.contacts),
            )
        for attr, value in filters:
            table, column = attr.split('.', 1)
            if table == 'zipabox':
                cls = models.Zipabox
            elif table == 'zipazone':
                cls = models.Zipazone
            elif table == 'zipacontact':
                cls = models.Zipacontact
            else:
                msg = 'Current Mask can\'t connect to the table '
                msg += table
                raise AttributeError(msg)
            if value.startswith('§'):
                try:
                    value = int(value[1:])
                except ValueError:
                    continue
                q = q.group_by(models.Zipabox).\
                    having(func.count(getattr(cls, column)) == value)
            else:
                q = q.filter(getattr(cls, column).op('regexp')(value))
        return q

    def get_custom(self, serial):
        session = models.Session()
        q = session.query(models.Zipacustom).\
            filter(models.Zipacustom.box_serial == serial)
        return q.first()

    def synchronize(self, user, password,
                    status_callback=None, error_callback=None):
        def sync_box(user, password, box, connection):
            session = self.get_session(user, password)
            serial = box['serial']
            self.select_box(session, serial)
            box = self.get_full_box_info(session)

            boxm = models.Zipabox(
                serial=serial,
                name=box['name'],
                online=box['online'],
                need_sync=box['needSync'],
                sync_date=box['syncDate'],
                firmware_version=box['firmwareVersion'],
            )
            for contact in box['contacts']:
                contactm = models.Zipacontact(
                    name=contact['name'],
                    surname=contact['surname'],
                    email=contact['email'],
                    phone=contact['phone'],
                    mobile_phone=contact['phoneMobile'],
                )
                boxm.contacts.append(contactm)
            connection.add(boxm)

            threads = []
            for j, zone in enumerate(box['zones']):
                thr = threading.Thread(
                    target=sync_zone,
                    name='ZoneThread-' + str(j),
                    args=(session, box, zone, connection),
                )
                thr.start()
                threads.append(thr)
            for thr in threads:
                thr.join()

        def sync_zone(session, box, zone, connection):
            try:
                device = self.get_device(session, zone['device']['uuid'])
                zonem = models.Zipazone(
                    uuid=zone['uuid'],
                    box_serial=box['serial'],
                    name=zone['name'],
                    ready=zone['state']['ready'],
                    armed=zone['state']['armed'],
                    tripped=zone['state']['tripped'],
                    bypassed=zone['state']['bypassed'],
                    sensor_state=zone['state']['sensorState'],
                    battery_level=device['state']['batteryLevel'],
                    battery_timestamp=device['state']['batteryTimestamp'],
                )
            except KeyError as e:
                log.warning(
                    'Faulty zone {} in {}\nError: {}'.format(
                        zone['name'],
                        box['name'],
                        str(e)
                    )
                )
                if error_callback:
                    error_callback(box['name'])
            else:
                connection.add(zonem)

        connection = models.Session()
        main_session = self.get_session(user, password)
        boxes = self.get_all_boxes(main_session)

        connection.query(models.Zipabox).delete()
        connection.query(models.Zipazone).delete()
        connection.commit()

        boxes_tally = len(boxes)
        threads = []
        max_requests = settings.get('max_concurrent_requests', 'zipato')
        for i, box in enumerate(boxes):
            if 'name' not in box:
                continue
            box_name = box['name']
            thr = threading.Thread(
                target=sync_box,
                name='BoxThread-' + str(i),
                args=(user, password, box, connection)
            )
            thr.start()
            threads.append(thr)

            if status_callback:
                status_callback(box_name, i+1, boxes_tally)
            while custom_requests.Session.opened > max_requests:
                continue

        for thr in threading.enumerate():
            try:
                thr.join()
            except RuntimeError:
                continue
        connection.commit()

    def edit_custom(self, serial, data):
        session = models.Session()

        custom = self.get_custom(serial)
        custom.comment = data['zipabox.comment']

        session.commit()

    def get_session(self, user, password):
        session = custom_requests.Session()
        password = sha1(password.encode('utf-8')).hexdigest()

        proxies = settings.get('proxies', 'zipato')
        uri = settings.get('uri', 'zipato')

        # get token
        path = '/zipato-web/rest/user/init'
        try:
            response = session.get(uri + path, proxies=proxies)
        except requests.exceptions.ConnectionError:
            return False
        nonce = response.json()['nonce']
        token = sha1((nonce + password).encode('utf-8')).hexdigest()

        # login
        path = '/zipato-web/json/Login'
        response = session.post(
            url=uri+path,
            data={
                'username': user,
                'password': token,
                'method': 'SHA1',
            },
            proxies=proxies,
        )

        response = response.json()
        if response['success']:
            return session
        else:
            return None

    def get_all_boxes(self, session):
        proxies = settings.get('proxies', 'zipato')
        uri = settings.get('uri', 'zipato')

        path = '/zipato-web/v2/box/list'
        response = session.get(uri + path, proxies=proxies)
        return response.json()

    def select_box(self, session, serial):
        proxies = settings.get('proxies', 'zipato')
        uri = settings.get('uri', 'zipato')

        path = '/zipato-web/v2/box/select?serial=' + serial
        session.get(uri + path, proxies=proxies)

    def get_full_box_info(self, session):
        proxies = settings.get('proxies', 'zipato')
        uri = settings.get('uri', 'zipato')

        path = '/zipato-web/v2/box'
        box = session.get(uri + path, proxies=proxies)
        box = box.json()

        path = '/zipato-web/v2/alarm/partitions'
        partitions = session.get(uri + path, proxies=proxies)
        partitions = partitions.json()

        path = '/zipato-web/v2/contacts'
        contacts = session.get(uri + path, proxies=proxies)
        contacts = contacts.json()

        box['contacts'] = contacts

        for partition in partitions:
            link = partition['link'] + '/zones'
            zones = session.get(link, proxies=proxies)
            zones = zones.json()
            box['zones'] = []
            for zone in zones:
                zlink = zone['link']
                link = zlink + (
                    '?network=false&device=true&'
                    'endpoint=false&clusterEndpoint=false&attribute=true&'
                    'config=false&value=true&state=true&full=false'
                )
                zone = session.get(link, proxies=proxies)
                zone = zone.json()
                if 'error' in zone.keys():
                    log.warning(
                        'Was not able to get zone {} in {}.'
                        '\nError message: {}'.format(
                            zlink,
                            box['name'],
                            zone['error'],
                        )
                    )
                else:
                    box['zones'].append(zone)

        return box

    def get_device(self, session, uuid):
        proxies = settings.get('proxies', 'zipato')
        uri = settings.get('uri', 'zipato')

        path = '/zipato-web/v2/devices/{}?state=true'.format(uuid)
        device = session.get(uri + path, proxies=proxies)
        device = device.json()

        return device
