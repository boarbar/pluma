

from sqlalchemy.orm import relationship
from sqlalchemy import String, Integer, Boolean
from sqlalchemy import Column, ForeignKey, Table

from base.models import Model, Session


association_table = Table(
    'relation_zipabox_zipacontact', Model.metadata,
    Column('contact_id', Integer, ForeignKey('zipacontact.id')),
    Column('box_id', Integer, ForeignKey('zipabox.id'))
)


class Zipabox(Model):
    __tablename__ = 'zipabox'
    id = Column(Integer, primary_key=True, nullable=False)
    serial = Column(String, nullable=False)
    name = Column(String, nullable=False)
    online = Column(Boolean, nullable=False)
    need_sync = Column(Boolean, nullable=False)
    sync_date = Column(String)
    firmware_version = Column(String)

    zones = relationship('Zipazone', back_populates='box', lazy='joined')
    contacts = relationship(
        'Zipacontact',
        secondary=association_table,
        back_populates='boxes',
    )

    def __repr__(self):
        r = (
            'id={}'.format(self.id),
            'serial={}'.format(self.serial),
            'name={}'.format(self.name),
            'online={}'.format(self.online),
            'need_sync={}'.format(self.need_sync),
            'zones={}'.format(self.zones),
        )
        return '<Zipabox(' + ', '.join(r) + ')>'


class Zipazone(Model):
    __tablename__ = 'zipazone'
    id = Column(Integer, primary_key=True, nullable=False)
    uuid = Column(String, nullable=False)
    box_serial = Column(String, ForeignKey('zipabox.serial'))
    name = Column(String, nullable=False)
    ready = Column(Boolean, nullable=False)
    armed = Column(Boolean, nullable=False)
    tripped = Column(Boolean, nullable=False)
    bypassed = Column(Boolean, nullable=False)
    sensor_state = Column(Boolean, nullable=False)
    battery_level = Column(Integer, nullable=False)
    battery_timestamp = Column(String, nullable=False)

    box = relationship('Zipabox', back_populates='zones')

    def __repr__(self):
        r = (
            'id={}'.format(self.id),
            'uuid={}'.format(self.uuid),
            'box_serial={}'.format(self.box_serial),
            'name={}'.format(self.name),
            'ready={}'.format(self.ready),
            'armed={}'.format(self.armed),
            'tripped={}'.format(self.tripped),
            'bypassed={}'.format(self.bypassed),
            'sensor_state={}'.format(self.sensor_state),
            'battery_level={}'.format(self.battery_level),
            'battery_timestamp={}'.format(self.battery_timestamp),
            'box={}'.format(self.box),
        )
        return '<Zipazone(' + ', '.join(r) + ')>'


class Zipacontact(Model):
    __tablename__ = 'zipacontact'
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String)
    surname = Column(String)
    email = Column(String)
    phone = Column(String)
    mobile_phone = Column(String)

    boxes = relationship(
        'Zipabox',
        secondary=association_table,
        back_populates='contacts',
    )


class Zipacustom(Model):
    __tablename__ = 'zipacustom'
    box_serial = Column(String, primary_key=True, nullable=False)
    comment = Column(String)
