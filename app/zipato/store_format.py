

import settings
from localisation import _


TAGS = settings.get('statuses', 'zipato')


def get(store, comment):
    def true_false(value):
        return _('zipato.wtrue' if value else 'zipato.wfalse')

    def zone_tag(battery_level):
        if battery_level < 1:
            return 'red'
        elif battery_level <= 10:
            return 'yellow'
        elif battery_level <= 50:
            return 'blue'
        else:
            return 'green'

    def status(online, has_users):
        if online:
            if has_users:
                return 'green'
            else:
                return 'blue'
        else:
            if has_users:
                return 'red'
            else:
                return 'yellow'

    zones = store.zones
    contacts = store.contacts
    return {
        'key': 'name-' + str(store.id),
        'name': store.name,
        'value': ('online' if store.online else 'offline') + '\t' +
            _('zipabox.zones') + ': ' + str(len(zones)) + '\t' +
            _('zipabox.contacts') + ': ' + str(len(contacts)) + '\t' +
            comment if comment else '',
        'tag': status(store.online, len(contacts) > 2),
        'nodes': [{
            'key': 'online-' + str(store.id),
            'name': _('zipabox.online'),
            'value': true_false(store.online),
            }, {
            'key': 'need_sync-' + str(store.id),
            'name': _('zipabox.need_sync'),
            'value': true_false(store.need_sync),
            }, {
            'key': 'sync_date-' + str(store.id),
            'name': _('zipabox.sync_date'),
            'value': store.sync_date,
            }, {
            'key': 'firmware_version-' + str(store.id),
            'name': _('zipabox.firmware_version'),
            'value': store.firmware_version,
            }, {
            'key': 'zones-' + str(store.id),
            'name': _('zipabox.zones'),
            'value': str(len(zones)),
            'nodes': [{
                'key': 'zone-' + str(zone.id),
                'name': zone.name,
                'value': '',
                'tag': zone_tag(zone.battery_level),
                'nodes': [{
                    'key': 'ready-' + str(zone.id),
                    'name': _('zipazone.ready'),
                    'value': true_false(zone.ready),
                    }, {
                    'key': 'armed' + str(zone.id),
                    'name': _('zipazone.armed'),
                    'value': true_false(zone.armed),
                    }, {
                    'key': 'tripped' + str(zone.id),
                    'name': _('zipazone.tripped'),
                    'value': true_false(zone.tripped),
                    }, {
                    'key': 'bypassed' + str(zone.id),
                    'name': _('zipazone.bypassed'),
                    'value': true_false(zone.bypassed),
                    }, {
                    'key': 'sensor_state' + str(zone.id),
                    'name': _('zipazone.sensor_state'),
                    'value': true_false(zone.sensor_state),
                    }, {
                    'key': 'battery_level' + str(zone.id),
                    'name': _('zipazone.battery_level'),
                    'value': zone.battery_level,
                    }, {
                    'key': 'battery_timestamp' + str(zone.id),
                    'name': _('zipazone.battery_timestamp'),
                    'value': zone.battery_timestamp,
                }]
            } for zone in zones],
            }, {
            'key': 'contacts-' + str(store.id),
            'name': _('zipabox.contacts'),
            'value': str(len(contacts)),
            'nodes': [{
                'key': 'contact-' + str(contact.id),
                'name': contact.name + ' ' + contact.surname,
                'value': '',
                'nodes': [{
                    'key': 'contact_name-' + str(contact.id),
                    'name': _('zipacontact.name'),
                    'value': contact.name,
                    }, {
                    'key': 'contact_surname-' + str(contact.id),
                    'name': _('zipacontact.surname'),
                    'value': contact.surname,
                    }, {
                    'key': 'contact_email-' + str(contact.id),
                    'name': _('zipacontact.email'),
                    'value': contact.email,
                    }, {
                    'key': 'contact_phone-' + str(contact.id),
                    'name': _('zipacontact.phone'),
                    'value': contact.phone,
                    }, {
                    'key': 'contact_mobile_phone-' + str(contact.id),
                    'name': _('zipacontact.mobile_phone'),
                    'value': contact.mobile_phone,
                }]
            } for contact in contacts],
        }]
    }
