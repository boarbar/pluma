

from base.interface import Interface
from base.models import ExcelSheet
from . import models
import settings


class Ordering(Interface):
    def __init__(self):
        super().__init__()
        self.MAP['init_from_excel'] = self.init_from_excel
        self.MAP['get_article'] = self.get_article
        self.MAP['get_search_criteria'] = self.get_search_criteria
        self.MAP['get_dossier'] = self.get_dossier
        self.MAP['get_filtered_dossier'] = self.get_filtered_dossier
        self.MAP['get_storage_tally'] = self.get_storage_tally
        self.MAP['get_logsys_macro_template'] = self.get_logsys_macro_template
        self.MAP['get_total_price'] = self.get_total_price
        self.MAP['get_needed_tally'] = self.get_needed_tally
        self.MAP['add_order'] = self.add_order
        self.MAP['edit_order'] = self.edit_order
        self.MAP['update_storage'] = self.update_storage

    def get_search_criteria(self):
        return (
            'orders.number', 'orders.status', 'orders.month', 'orders.year'
        )

    def init_from_excel(self):
        raise NotImplementedError

    def get_article(self, number):
        session = models.Session()
        return session.query(models.StockList).filter_by(number=number).first()

    def get_dossier(self):
        session = models.Session()
        return session.\
            query(models.Order).\
            join(models.StockList).\
            all()

    def get_filtered_dossier(self, filters):
        session = models.Session()
        q = session.query(models.Order).\
            join(models.StockList)

        for attr, value in filters:
            table, column = attr.split('.', 1)
            if table == 'orders':
                cls = models.Order
            elif table == 'stocklist':
                cls = models.StockList
            else:
                msg = 'Current Mask can\'t connect to the table '
                msg += table
                raise AttributeError(msg)
            q = q.filter(getattr(cls, column).op('regexp')(value))
        return q

    def get_storage_tally(self, number):
        session = models.Session()
        articles = session.query(models.Storage).\
            filter(models.Storage.number == number)
        return sum([int(a.amount.split(',')[0]) for a in articles])

    def get_logsys_macro_template(self, name):
        path = 'app/ordering/templates/logsys_macro_{}_template.txt'
        path = path.format(name)
        with open(path, 'r') as f:
            content = f.read()
        return content

    def get_total_price(self, filters):
        q = self.get_filtered_dossier(filters)
        price = 0.0
        for order in q:
            stock_article = order.stock_article
            price += stock_article.price * order.amount

        return price

    def get_needed_tally(self, article):
        tsettings = settings.get_object('ordering')
        type1_buffer = tsettings['type1_buffer']
        type1_planned = tsettings['type1_planned']
        type2_buffer = tsettings['type2_buffer']
        type2_planned = tsettings['type2_planned']
        type3_buffer = tsettings['type3_buffer']
        type3_planned = tsettings['type3_planned']
        type4_buffer = tsettings['type4_buffer']
        type4_planned = tsettings['type4_planned']
        u_h_buffer = tsettings['u_h_buffer']
        u_h_planned = tsettings['u_h_planned']
        u_t_buffer = tsettings['u_t_buffer']
        u_t_planned = tsettings['u_t_planned']

        needed = 0
        needed += (type1_buffer + type1_planned) * int(article.type1)
        needed += (type2_buffer + type2_planned) * int(article.type2)
        needed += (type3_buffer + type3_planned) * int(article.type3)
        needed += (type4_buffer + type4_planned) * int(article.type4)
        needed += (u_h_buffer + u_h_planned) * int(article.refit_honeywell)
        needed += (u_t_buffer + u_t_planned) * int(article.refit_telenot)
        return round(needed)

    def add_order(self, **kwargs):
        order = models.Order(**kwargs)
        session = models.Session()
        session.add(order)
        session.commit()

    def edit_order(self, id_, changes):
        session = models.Session()
        order = session.query(models.Order).filter_by(id=id_).first()
        for attr, value in changes.items():
            attr = attr.split('.')[-1]
            setattr(order, attr, value)
        session.commit()

    def update_storage(self, path):
        session = models.Session()
        session.query(models.Storage).delete()
        session.commit()

        sheet = 'Tabelle1'
        data_range = 'A1:U2000'
        headers = [
            ('A', 'location', 'Lagerort'),
            ('B', 'storage', '???'),
            ('C', 'place', '???'),
            ('D', 'allocation', '???'),
            ('E', 'number', 'Number'),
            ('F', 'prod_desc', 'Produktbeschreibung'),
            ('G', 'desc', 'Beschreibung'),
            ('H', 'serial', 'Seriennummer'),
            ('I', 'amount', 'Menge'),
            ('J', 'price', 'Preis'),
            ('K', 'holdback', 'Sperrfrist'),
            ('L', 'expiry_date', 'Ablaufdatum'),
            ('M', 'swap_nr', '???'),
            ('N', 'job_nr', '???'),
            ('O', 'pos', '???'),
        ]
        excel = ExcelSheet.load(path, sheet, data_range, headers)
        for item in excel.data:
            data = {key: item[key] for row, key, info in headers}
            storage = models.Storage(**data)
            session.add(storage)
        session.commit()
