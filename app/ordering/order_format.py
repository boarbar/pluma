

from lib.util.text import fill

import settings
from localisation import _


TAGS = settings.get('statuses', 'ordering')


def get(order, article, storage_tally, needed):
    try:
        description = article.description.decode('utf-8')
    except AttributeError:
        description = article.description
    remaining = storage_tally - needed
    new_stored = remaining + order.amount
    title = fill(
        _('orders.amount') + ': ' + str(order.amount) + '{f}',
        mod_after=30,
        ) + '\t'
    title += fill(
        _('calc.stored') + ': ' + str(storage_tally) + '{f}',
        mod_after=30,
        ) + '\t'
    title += _('calc.new_stored') + ': ' + str(new_stored)
    return {
        'key': 'number-' + str(order.id),
        'name': order.number,
        'value': title,
        'tag': TAGS[order.status],
        'nodes': [{
            # article number
            'key': 'dnumber-' + str(order.id),
            'name': _('orders.number'),
            'value': order.number,
            }, {
            # article name
            'key': 'name-' + str(order.id),
            'name': _('orders.name'),
            'value': description,
            }, {
            # stored article count
            'key': 'calc.stored-' + str(order.id),
            'name': _('calc.stored'),
            'value': storage_tally,
            }, {
            # needed article count
            'key': 'calc.needed-' + str(order.id),
            'name': _('calc.needed'),
            'value': needed,
            }, {
            # remaining article count
            'key': 'calc.remaining-' + str(order.id),
            'name': _('calc.remaining'),
            'value': remaining,
            }, {
            # total number of ordered articles
            'key': 'order.amount-' + str(order.id),
            'name': _('orders.amount'),
            'value': order.amount,
            }, {
            # amount stored after order arrival
            'key': 'calc.new_stored-' + str(order.id),
            'name': _('calc.new_stored'),
            'value': new_stored,
            }, {
            # minimum amount stored
            'key': 'order.min_stored-' + str(order.id),
            'name': _('orders.min_stored'),
            'value': article.min_stored,
            }, {
            # total cost
            'key': 'calc.cost-' + str(order.id),
            'name': _('calc.cost'),
            'value': storage_tally * article.price,
            }, {
            # status of order
            'key': 'order.status-' + str(order.id),
            'name': _('orders.status'),
            'value': _(order.status),
            }, {
            # month of order
            'key': 'order.month-' + str(order.id),
            'name': _('orders.month'),
            'value': order.month,
            }, {
            # year of order
            'key': 'order.year-' + str(order.id),
            'name': _('orders.year'),
            'value': order.year,
        }]
    }
