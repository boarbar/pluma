

import datetime
import math
from collections import OrderedDict

from tkinter import filedialog, StringVar
from tkinter.ttk import Label, Entry, Separator

import settings
from localisation import _
from lib.util.check import is_int
from base.templates import Dossier
from .interface import Ordering
from base.templates.inputs import DetailInput, item
from . import order_format
from base.models import StockList


class View(Dossier):
    def __init__(self):
        interface = Ordering()
        self.settings = settings.get_object('ordering')
        super().__init__(interface, 'ordering')
        self.title('ProjectPluma - ' + _('masks.ordering'))

    def _format_dossier(self, dossier):
        data = []

        for order in dossier:
            article = self.interface('get_article', number=order.number)
            storage_tally = self.interface('get_storage_tally', order.number)
            needed = self.interface('get_needed_tally', article)
            odr = order_format.get(
                order,
                article,
                storage_tally,
                needed,
            )
            data.append(odr)
        return data

    def init_search(self):
        super().init_search()

        date = datetime.datetime.now()
        field = 'orders.month'
        criteria = str(date.month)
        self.add_filter(field, criteria, pin=True)
        field = 'orders.year'
        criteria = str(date.year)
        self.add_filter(field, criteria, pin=True)

    def init_actions(self):
        super().init_actions()

        self.f_action.s_default_addition = Separator(
            self.f_action,
            orient='horizontal'
        )

        self.f_action.d_type1 = Label(
            self.f_action,
            text=_('ui.type1')
        )
        var = StringVar(value=self.settings['type1_planned'])
        self.f_action.w_type1_planned = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_type1_planned.var = var
        var = StringVar(value=self.settings['type1_buffer'])
        self.f_action.w_type1_buffer = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_type1_buffer.var = var

        self.f_action.d_type2 = Label(
            self.f_action,
            text=_('ui.type2')
        )
        var = StringVar(value=self.settings['type2_planned'])
        self.f_action.w_type2_planned = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_type2_planned.var = var
        var = StringVar(value=self.settings['type2_buffer'])
        self.f_action.w_type2_buffer = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_type2_buffer.var = var

        self.f_action.d_type3 = Label(
            self.f_action,
            text=_('ui.type3')
        )
        var = StringVar(value=self.settings['type3_planned'])
        self.f_action.w_type3_planned = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_type3_planned.var = var
        var = StringVar(value=self.settings['type3_buffer'])
        self.f_action.w_type3_buffer = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_type3_buffer.var = var

        self.f_action.d_type4 = Label(
            self.f_action,
            text=_('ui.type4')
        )
        var = StringVar(value=self.settings['type4_planned'])
        self.f_action.w_type4_planned = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_type4_planned.var = var
        var = StringVar(value=self.settings['type4_buffer'])
        self.f_action.w_type4_buffer = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_type4_buffer.var = var

        self.f_action.d_u_t = Label(
            self.f_action,
            text=_('ui.u_t')
        )
        var = StringVar(value=self.settings['u_t_planned'])
        self.f_action.w_u_t_planned = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_u_t_planned.var = var
        var = StringVar(value=self.settings['u_t_buffer'])
        self.f_action.w_u_t_buffer = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_u_t_buffer.var = var

        self.f_action.d_u_h = Label(
            self.f_action,
            text=_('ui.u_h')
        )
        var = StringVar(value=self.settings['u_h_planned'])
        self.f_action.w_u_h_planned = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_u_h_planned.var = var
        var = StringVar(value=self.settings['u_h_buffer'])
        self.f_action.w_u_h_buffer = Entry(
            self.f_action,
            textvariable=var
        )
        self.f_action.w_u_h_buffer.var = var

        self.f_action.d_type.grid(row=0, column=0)
        self.f_action.w_type.grid(row=1, column=0)
        self.f_action.s_default_addition.grid(
            row=2, column=0, columnspan=3,
            sticky='nswe', pady=5,
        )
        self.f_action.d_type1.grid(row=3, column=0, sticky='nw')
        self.f_action.w_type1_planned.grid(row=3, column=1, sticky='nwe')
        self.f_action.w_type1_buffer.grid(row=3, column=2, sticky='nwe')
        self.f_action.d_type2.grid(row=4, column=0, sticky='nw')
        self.f_action.w_type2_planned.grid(row=4, column=1, sticky='nwe')
        self.f_action.w_type2_buffer.grid(row=4, column=2, sticky='nwe')
        self.f_action.d_type3.grid(row=5, column=0, sticky='nw')
        self.f_action.w_type3_planned.grid(row=5, column=1, sticky='nwe')
        self.f_action.w_type3_buffer.grid(row=5, column=2, sticky='nwe')
        self.f_action.d_type4.grid(row=6, column=0, sticky='nw')
        self.f_action.w_type4_planned.grid(row=6, column=1, sticky='nwe')
        self.f_action.w_type4_buffer.grid(row=6, column=2, sticky='nwe')
        self.f_action.d_u_t.grid(row=7, column=0, sticky='nw')
        self.f_action.w_u_t_planned.grid(row=7, column=1, sticky='nwe')
        self.f_action.w_u_t_buffer.grid(row=7, column=2, sticky='nwe')
        self.f_action.d_u_h.grid(row=8, column=0, sticky='nw')
        self.f_action.w_u_h_planned.grid(row=8, column=1, sticky='nwe')
        self.f_action.w_u_h_buffer.grid(row=8, column=2, sticky='nwe')

    def init_options(self):
        super().init_options()

        self.f_options.w_options = DetailInput(self.f_options)

    def on_exit(self):
        super().on_exit()

        self._save_fields()

    def search(self):
        super().search()

        filters = self.get_filters()
        price = self.interface('get_total_price', filters)

        price = self.interface.format_currency(price)
        self.f_status.add('pprice', _('ui.pprice_tally') + ' ' + price)

    def add_filter(self, field=None, criteria=None, pin=False, refilter=False):
        if field is None:
            field = self.f_search.w_field.get()
        if field == 'orders.status':
            statuses = settings.get('statuses', self.name)
            statuses = {_(s): s for s in statuses}
            if criteria is None:
                criteria = self.f_search.w_criteria.get()
            if criteria in statuses:
                criteria = statuses[criteria]
                self.f_search.w_criteria.var.set(criteria)
        super().add_filter(
            field=field, criteria=criteria, pin=pin, refilter=refilter
        )

    def make_action(self):
        self._save_fields()
        super().make_action()

    def _save_fields(self):
        value = self.f_action.w_type1_planned.var.get()
        settings.write('type1_planned', int(value), self.name)
        value = self.f_action.w_type1_buffer.var.get()
        settings.write('type1_buffer', int(value), self.name)
        value = self.f_action.w_type2_planned.var.get()
        settings.write('type2_planned', int(value), self.name)
        value = self.f_action.w_type2_buffer.var.get()
        settings.write('type2_buffer', int(value), self.name)
        value = self.f_action.w_type3_planned.var.get()
        settings.write('type3_planned', int(value), self.name)
        value = self.f_action.w_type3_buffer.var.get()
        settings.write('type3_buffer', int(value), self.name)
        value = self.f_action.w_type4_planned.var.get()
        settings.write('type4_planned', int(value), self.name)
        value = self.f_action.w_type4_buffer.var.get()
        settings.write('type4_buffer', int(value), self.name)
        value = self.f_action.w_u_t_planned.var.get()
        settings.write('u_t_planned', int(value), self.name)
        value = self.f_action.w_u_t_buffer.var.get()
        settings.write('u_t_buffer', int(value), self.name)
        value = self.f_action.w_u_h_planned.var.get()
        settings.write('u_h_planned', int(value), self.name)
        value = self.f_action.w_u_h_buffer.var.get()
        settings.write('u_h_buffer', int(value), self.name)

    def place_order(self):
        path = filedialog.askopenfilename()
        if not path:
            return

        try:
            self.interface('update_storage', path)
        except Exception as e:
            title = _('err.title.error_while_processing_file')
            msg = _('err.msg.error_while_processing_file')
            msg = '\n'.join((msg, str(e)))
            self.show_error(title, msg)
            return

        articles_settings = self.settings['articles']
        date = datetime.datetime.now()

        filters = [
            ['orders.month', date.month],
            ['orders.year', date.year],
        ]
        current_orders = self.interface('get_filtered_dossier', filters)
        current_orders = [odr.number for odr in current_orders]

        for number in articles_settings:
            if number in current_orders:
                continue
            article = self.interface('get_article', number)
            stored = self.interface('get_storage_tally', number)
            needed = self.interface('get_needed_tally', article)
            amount = max(article.min_stored - (stored - needed), 0)
            amount = math.ceil(amount / 5) * 5
            order = {
                'number': number,
                'amount': amount,
                'status': 'open',
                'month': str(date.month),
                'year': str(date.year),
            }
            self.interface('add_order', **order)

        self.search()

    def _edit_order(self, id_, callback, default_status=None):
        focused = self.f_tree.w_dossier.focus()
        children = self.f_tree.w_dossier.get_children(focused)
        if children:
            self.f_tree.w_dossier.see(children[0])

        order = self.get_filtered()[0]

        statuses = settings.get('statuses', self.name)
        statuses = {_(s): s for s in statuses}
        try:
            description = order.stock_article.description.decode('utf-8')
        except AttributeError:
            description = order.stock_article.description
        order = OrderedDict([
            item(
                key='orders.number',
                value=order.number,
                editable=False,
            ),
            item(
                key='orders.name',
                value=description,
                editable=False,
            ),
            item(
                key='orders.amount',
                value=order.amount,
                validity=is_int,
                val_msg=_('info.must_be_int'),
            ),
            item(
                key='orders.status',
                default=_(default_status if default_status else order.status),
                values=statuses,
                validity=lambda w: w in statuses.keys(),
                val_msg=_('info.must_be_in_list'),
            ),
        ])

        self.show_options(
            title=_('ui.edit_order'),
            callback=callback,
            items=order,
            selected='orders.status',
        )

    def edit_order(self):
        id_ = self.get_selected_id()
        if id_ is None:
            return
        self.add_filter('orders.id', str(id_))

        def on_finish(data):
            self.interface('edit_order', id_, data)
            self.f_search.w_filters.clear()
            self.search()

        self._edit_order(id_, on_finish)
        self.search()

    def edit_all_filtered_orders(self):
        def on_finish(id_, data):
            self.interface('edit_order', id_, data)
            self.edit_all_filtered_orders()

        if 'orders.status' not in self.f_search.w_filters.items:
            self.add_filter('orders.status', 'open')

        order = self.get_filtered().first()
        if order:
            self._edit_order(
                id_=order.id,
                callback=lambda data: on_finish(order.id, data),
                default_status='confirmed',
            )
        else:
            self.search()

    def export_order(self):
        orders = self.get_filtered().order_by(StockList.deliverer)
        macro = []
        deliverer_to_storage = settings.get('deliverer_conv', self.name)

        last_deliverer = ''
        for order in orders:
            if order.amount == 0:
                continue
            if order.status != 'confirmed':
                title = _('err.title.order_not_confirmed')
                msg = _('err.msg.order_not_confirmed')
                self.show_error(title, msg)
                return

            article = self.interface('get_article', order.number)
            sign = ' '.join((
                _('month.m{}'.format(str(order.month))),
                str(order.year)
            ))

            if article.deliverer != last_deliverer:
                last_deliverer = article.deliverer
                template = self.interface(
                    'get_logsys_macro_template',
                    'dummy',
                )

                step = template.format(
                    deliverer_nr=article.deliverer,
                    number=article.number,
                    ek_type='E',
                    your_sign1=sign,
                    cost_location='011070',
                    amount=order.amount,
                    shipping_type='K',
                    delivery_date=(
                        datetime.date.today() +
                        datetime.timedelta(days=5)
                    )
                )
            else:
                template = self.interface(
                    'get_logsys_macro_template',
                    'article',
                )
                # if article.deliverer is None:
                #     deliverer = ''
                # else:
                #     deliverer = article.deliverer
                # try:
                #     storage_nr = deliverer_to_storage[deliverer]
                # except KeyError:
                #     storage_nr = ''

                step = template.format(
                    number=order.number,
                    ek_type='E',
                    your_sign1=sign,
                    cost_location='011070',
                    amount=order.amount,
                    delivery_date=(
                        datetime.date.today() +
                        datetime.timedelta(days=5)
                    )
                )
            macro.append(step)

        macro = '\n'.join(macro)

        path = filedialog.asksaveasfilename(
            title=_('ui.save_location'),
            defaultextension='.lsm',
            filetypes=[('LogSys Macro', '*.lsm')],
        )
        with open(path, 'w') as f:
            f.write(macro)

        title = _('ask.title.macro_executed')
        msg = _('ask.msg.macro_executed')
        answer = self.ask_question(title, msg)
        if answer == 'yes':
            orders = self.get_filtered()
            for order in orders:
                if order.status != 'confirmed':
                    continue
                self.interface(
                    'edit_order', order.id,
                    {'order.status': 'ordered'},
                )
            self.search()

    def get_actions(self):
        def export(kind, data):
            kind = 'export_' + kind
            result = self.interface(kind, data)
            if result == 'PermissionError':
                ttl = 'err.title.permission_denied'
                msg = 'err.msg.permission_denied'
                self.show_error(_(ttl), _(msg))

        return OrderedDict([
            ['place_order', self.place_order],
            ['edit_order', self.edit_order],
            ['edit_all_filtered_orders', self.edit_all_filtered_orders],
            ['export_order', self.export_order],
        ])
