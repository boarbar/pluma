

from sqlalchemy.orm import relationship
from sqlalchemy import String, Integer, Float
from sqlalchemy import Column, ForeignKey
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import settings
from lib.util import logsys_util

db_type = settings.get('db', 'ordering')
db_path = settings.get('db_path', 'ordering')
engine = sqlalchemy.create_engine(
    db_type + db_path,
    connect_args={'check_same_thread': False},
)
conn = engine.connect()


@sqlalchemy.event.listens_for(engine, 'begin')
def do_begin(conn):
    conn.connection.create_function('regexp', 2, logsys_util.match)

Session = sessionmaker(bind=engine)
Model = declarative_base()


class Order(Model):
    __tablename__ = 'orders'
    id = Column(Integer, primary_key=True, nullable=False)
    number = Column(String, ForeignKey('stocklist.number'), nullable=False)
    amount = Column(Integer, nullable=False)
    status = Column(String, nullable=False)
    month = Column(Integer, nullable=False)
    year = Column(Integer, nullable=False)

    stock_article = relationship(
        'StockList', back_populates='orders', lazy='joined'
    )


class StockList(Model):
    __tablename__ = 'stocklist'
    number = Column(String, primary_key=True)
    description = Column(String)
    price = Column(Float)
    comment = Column(String)
    type1 = Column(Integer)
    type2 = Column(Integer)
    type3 = Column(Integer)
    type4 = Column(Integer)
    refit_honeywell = Column(Integer)
    refit_telenot = Column(Integer)
    min_stored = Column(Integer)
    deliverer = Column(String)

    orders = relationship('Order', back_populates='stock_article')


class Storage(Model):
    __tablename__ = 'storage'
    id = Column(Integer, primary_key=True, nullable=False)
    location = Column(String, nullable=False)
    storage = Column(String, nullable=False)
    place = Column(String, nullable=False)
    allocation = Column(String)
    number = Column(String, nullable=False)
    prod_desc = Column(String)
    desc = Column(String)
    serial = Column(String)
    amount = Column(Integer, nullable=False)
    price = Column(String, nullable=False)
    holdback = Column(String)
    expiry_date = Column(String)
    swap_nr = Column(String)
    job_nr = Column(String)
    pos = Column(String)
