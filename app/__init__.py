

import os
import importlib


APPS = {}
IGNORE = ['common']
app = None

for name in os.listdir('app'):
    if name in IGNORE or name.count('__') >= 2:
        continue
    title = name.title().replace('_', '')
    path = 'app.{}.view'.format(name)
    app = importlib.import_module(path)
    APPS[name] = app.View


def create_app(name):
    global app
    app = APPS[name]()
    return app
