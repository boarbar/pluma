

import datetime

from collections import OrderedDict

from tkinter import filedialog
from tkinter.ttk import Button

import settings
from localisation import _
from base.templates import Dossier
from .interface import Accounting
from lib.util import check
from base.templates.inputs import item
from . import dossier_format


class View(Dossier):
    def __init__(self):
        interface = Accounting()
        super().__init__(interface, 'accounting')
        self.title('ProjectPluma - ' + _('masks.accounting'))

    def _format_dossier(self, dossier):
        data = []

        for service in dossier:
            ser = dossier_format.get(service)
            data.append(ser)
        return data

    def init_menu(self, *args, **kwargs):
        def on_temp_rollout_init():
            self.interface.temp_rollout_init()
            self.search()
        super().init_menu(*args, **kwargs)

        self.f_menu.w_temp_rollout_reload = Button(
            self.f_menu,
            text=_('temp_rollout_init'),
            command=on_temp_rollout_init
        )

        self.f_menu.w_temp_rollout_reload.grid(row=0, column=2, sticky='ne')

    def add_filter(self, field=None, criteria=None, pin=False, refilter=False):
        if field is None:
            field = self.f_search.w_field.get()
        if field == 'articles.status':
            statuses = settings.get('statuses', self.name)
            statuses = {_(s): s for s in statuses}
            if criteria is None:
                criteria = self.f_search.w_criteria.get()
            if criteria in statuses:
                criteria = statuses[criteria]
                self.f_search.w_criteria.var.set(criteria)

        super().add_filter(
            field=field, criteria=criteria, pin=pin, refilter=refilter,
        )

    def search(self, *args, **kwargs):
        super().search(*args, **kwargs)

        filters = self.get_filters()
        price = self.interface('get_total_price', filters)

        price = self.interface.format_currency(price)
        self.f_status.add('pprice', _('ui.pprice_tally') + ' ' + price)

    def get_selected_article_id(self):
        iid = self.f_tree.w_dossier.focus()
        while iid != '':
            key, art_id = iid.split('-')
            if key == 'number':
                return art_id
            iid = self.f_tree.w_dossier.parent(iid)
        return None

    def add_article(self):
        ser_id = self.get_selected_id()
        if ser_id is None:
            return

        def on_finish(data):
            data = [
                data['articles.number'],
                ser_id,  # service_id
                data['articles.amount'],
                '',  # comment
                data['articles.month'],
                data['articles.status'],
                data['articles.list_nr'],
            ]
            self.interface('add_article', *data)
            self.search()

        all_articles = self.interface('get_all_article_numbers')
        all_articles = [[a[0], a[0]] for a in all_articles]
        all_articles = OrderedDict(all_articles)
        months = OrderedDict([[_('month.m' + str(m)), str(m)]
                              for m in range(1, 13)])
        statuses = settings.get('statuses', self.name)
        statuses = {_(s): s for s in statuses}
        lnrs = OrderedDict([[str(m), str(m)] for m in range(1, 8)])

        self.show_article_input(
            title=_('ui.add_article'),
            callback=on_finish,
            items=OrderedDict([
                item(
                    key='articles.number',
                    values=all_articles,
                    validity=lambda w: w in all_articles.keys(),
                    val_msg=_('info.must_be_in_list'),
                    auto_take=True
                ),
                item(
                    key='articles.amount',
                    validity=lambda w: check.is_float(w),
                    val_msg=_('info.must_be_float')
                ),
                item(
                    key='articles.month',
                    default=_('month.m' +
                              str(datetime.datetime.now().month)),
                    values=months,
                    validity=lambda w: w in months.keys(),
                    val_msg=_('info.must_be_in_list')
                ),
                item(
                    key='articles.status',
                    values=statuses,
                    default=_('open'),
                    validity=lambda w: w in statuses.keys(),
                    val_msg=_('info.must_be_in_list')
                ),
                item(
                    key='articles.list_nr',
                    default=1,
                    values=lnrs,
                    validity=lambda w: w in lnrs.keys(),
                    val_msg=_('info.must_be_in_list')
                )]
            ),
            selected='articles.amount',
        )

        criteria = str(ser_id)
        criteria = '^{}$'.format(criteria)
        self.add_filter('services.id', criteria, refilter=True)

    def edit_article(self):
        def on_finish(data):
            self.interface('edit_article', art_id, data)

        art_id = self.get_selected_article_id()
        if art_id is None:
            title = _('err.title.no_article_selected')
            msg = _('err.msg.no_article_selected')
            self.show_error(title, msg)
            return

        statuses = settings.get('statuses', self.name)
        statuses = {_(s): s for s in statuses}
        article = self.interface(
            'get_filtered_articles',
            [['articles.id', '^{}$'.format(str(art_id))]]
        ).first()
        self.show_article_input(
           title=_('ui.edit_article'),
           callback=on_finish,
           items=OrderedDict([
               item(
                   key='articles.status',
                   value=article.status,
                   values=statuses,
                   validity=lambda w: w in statuses.keys(),
                   val_msg=_('info.must_be_in_list')
               )]
           )
        )

    def show_article_input(self, **kwargs):
        focus = self.f_tree.w_dossier.focus()
        if focus:
            self.show_options(**kwargs)
        else:
            self.show_error(
                _('err.title.must_select_item_from_dossier'),
                _('err.msg.must_select_item_from_dossier')
            )

    def export_data_to_file(self, etype, data):
        etype = 'export_' + etype
        result = self.interface(etype, data)
        if result == 'PermissionError':
            ttl = 'err.title.permission_denied'
            msg = 'err.msg.permission_denied'
            self.show_error(_(ttl), _(msg))

    def export_sg322(self):
        def export_file():
            months = OrderedDict([[_('month.m' + str(m)), str(m)]
                                  for m in range(1, 13)])

            self.show_options(
                title=_('ui.export_sg322'),
                callback=lambda data: self.export_data_to_file('sg322', data),
                items=OrderedDict([
                    item(
                        key='articles.month',
                        values=months,
                        validity=lambda w: w in months.keys()
                    ),
                    item(
                        key='filepath',
                        special='filepath_chooser'
                    )]
                )
            )

        def export_macro():
            dossier = self.interface('get_dossier')
            macro = []

            for service in dossier:
                articles = service.articles
                for article in articles:
                    if article.status != 'open':
                        continue

                    template = self.interface(
                        'get_logsys_macro_template',
                        'sg322',
                    )

                    step = template.format(
                        object_nr=service.store.object_nr,
                        number=article.number,
                    )
                    macro.append(step)

            macro = '\n'.join(macro)

            path = filedialog.asksaveasfilename(
                title=_('ui.save_location'),
                defaultextension='.lsm',
                filetypes=[('LogSys Macro', '*.lsm')],
            )
            if path:
                with open(path, 'w') as f:
                    f.write(macro)

        export_file()
        export_macro()

    def export_as(self):
        def export_file():
            data = filedialog.askdirectory()
            if data:
                self.export_data_to_file('as', data)

        def export_macro(services, macro):
            template = self.interface(
                'get_logsys_macro_template',
                'as',
            )

            def callback(data):
                step = template.format(
                    number=data['articles.number'],
                    delivery_date=data['articles.delivery_date'],
                    amount=data['articles.amount'],
                )
                macro.insert(-1, step)
                export_macro(services, macro)

            while not services[0]['articles']:
                services.pop(0)
                if not services:
                    macro = '\n'.join(macro)
                    path = filedialog.asksaveasfilename(
                        title=_('ui.save_location'),
                        defaultextension='.lsm',
                        filetypes=[('LogSys Macro', '*.lsm')],
                    )
                    if path:
                        with open(path, 'w') as f:
                            f.write(macro)

                    title = _('ask.title.macro_executed')
                    msg = _('ask.msg.macro_executed')
                    answer = self.ask_question(title, msg)
                    if answer == 'yes':
                        for service in self.get_filtered():
                            for article in service.articles:
                                self.interface(
                                    'edit_article',
                                    article.id,
                                    {'articles.status': 'confirmed'}
                                )
                    return

            service = services[0]
            article = service['articles'].pop(0)

            if service['first']:
                dummy_template = self.interface(
                    'get_logsys_macro_template',
                    'as_dummy'
                )
                desc_template = self.interface(
                    'get_logsys_macro_template',
                    'desc'
                )
                dummy_article = dummy_template.format(
                    order_nr='Bestellt',
                    desc=desc_template.format(
                        store_name=service['store']['address1'],
                        street=service['store']['street'],
                        zip=service['store']['zip'],
                        city=service['store']['city'],
                    )
                ).replace('POSNEU', 'NEU')
                macro.append(dummy_article)
                macro.append('AUFTRAG_AKTIVIEREN\n')
                service['first'] = False

            self.show_options(
                title=_('ui.export_as'),
                callback=callback,
                items=OrderedDict([
                    item(
                        key='articles.number',
                        value=article['number'],
                    ),
                    item(
                        key='articles.amount',
                        value=article['amount'],
                    ),
                    item(
                        key='articles.delivery_date',
                    ),
                    item(
                        key='articles.order_nr',
                        value='Bestellt',
                    )
                ]),
                selected='articles.delivery_date',
            )

        self.add_filter('articles.status', 'open')
        services = self.get_filtered()
        export_file()
        services_dict = [
            {
                'first': True,
                'debit_date': service.debit_date,
                'articles': [
                    {
                        'number': article.number,
                        'amount': article.amount,
                    } for article in service.articles
                      if article.status == 'open'
                ],
                'store': {
                    'address1': service.store.address1,
                    'street': service.store.street,
                    'zip': service.store.zip,
                    'city': service.store.city,
                }
            } for service in services
        ]
        if services_dict:
            export_macro(services_dict, [])

    def get_actions(self):
        return OrderedDict([
            ['add_article', self.add_article],
            ['edit_article', self.edit_article],
            ['export_sg322', self.export_sg322],
            ['export_as', self.export_as],
        ])
