

import settings
from localisation import _


TAGS = settings.get('statuses', 'accounting')


def get(service):
    store = service.store
    store_address1 = ('' if store is None else store.address1)
    return {
        'key': 'service-' + str(service.id),
        'name': service.eks_id,
        'value': service.process + '-' + service.status + '-' + store_address1,
        'nodes': [{
            'key': 'details-' + str(service.id),
            'name': _('ui.details'),
            'value': '',
            'nodes': [{
                'key': 'service.eks_id-' + str(service.id),
                'name': _('services.eks_id'),
                'value': service.eks_id,
                }, {
                'key': 'service.address1-' + str(service.id),
                'name': _('stores.address1'),
                'value': store.address1,
                }, {
                'key': 'service.status-' + str(service.id),
                'name': _('services.status'),
                'value': service.status,
                }]
            }, {
            'key': 'articles-' + str(service.id),
            'name': _('ui.articles'),
            'value': '',
            'nodes': [{
                'key': 'number-' + str(article.id),
                'name': article.number,
                'value': str(article.amount) + '-' + article.comment,
                'tag': TAGS[article.status],
                'nodes': [{
                    'key': 'dnumber-' + str(article.id),
                    'name': _('articles.number'),
                    'value': article.number,
                    }, {
                    'key': 'article.status-' + str(article.id),
                    'name': _('articles.status'),
                    'value': _(article.status),
                    }, {
                    'key': 'article.amount-' + str(article.id),
                    'name': _('articles.amount'),
                    'value': article.amount,
                    }, {
                    'key': 'article.month-' + str(article.id),
                    'name': _('articles.month'),
                    'value': article.month,
                    }, {
                    'key': 'article.list-' + str(article.id),
                    'name': _('articles.list_nr'),
                    'value': article.list_nr,
                }],
            } for article in service.articles],
        }]
    }
