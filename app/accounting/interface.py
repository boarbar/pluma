

import os
import logging

import settings
from base.interface import Interface
from base import models


class Accounting(Interface):
    def __init__(self):
        super().__init__()
        self.MAP['init_from_excel'] = self.init_from_excel
        self.MAP['get_search_criteria'] = self.get_search_criteria
        self.MAP['get_dossier'] = self.get_dossier
        self.MAP['get_filtered_dossier'] = self.get_filtered_dossier
        self.MAP['get_filtered_stores'] = self.get_filtered_stores
        self.MAP['get_all_article_numbers'] = self.get_all_article_numbers
        self.MAP['get_filtered_articles'] = self.get_filtered_articles
        self.MAP['get_eks_id'] = self.get_eks_id
        self.MAP['get_total_price'] = self.get_total_price
        self.MAP['get_logsys_macro_template'] = self.get_logsys_macro_template
        self.MAP['add_article'] = self.add_article
        self.MAP['edit_article'] = self.edit_article
        self.MAP['export_sg322'] = self.export_sg322
        self.MAP['export_as'] = self.export_as

        if settings.get('temp_rollout_init_on_start', 'accounting'):
            self.temp_rollout_init()

    def temp_rollout_init(self):
        # TEMP till Rollout is completely in database
        path = r'\\FSWOL2\DPAG_Gefahrenmeldeanlage_012839 (01-WOL)\Rolloutübersicht GMA_2016.xlsm'
        sheet = 'Rolloutliste'
        drange = 'A3:CB2502'
        headers = [
            ('B', 'ifd_nr', 'Ifd. Nr.'),
            ('A', 'eks_id', 'EKS-ID'),
            ('G', 'process', 'Vorgang'),
            ('J', 'status', 'Status'),
            ('N', 'debit_date', 'Soll Termin'),
            ('P', 'actual_date', 'Ist Termin'),
            ('AS', 'orderer', 'Besteller'),
            ('BN', 'sys_nr', 'Anlagen-Nummer Neu'),
        ]
        required = ['eks_id', 'process', 'status']
        self.init_from_excel(path, sheet, drange, headers, required)

    def init_from_excel(self, path, sheet, data_range, headers, required):
        log = logging.getLogger('app.controller.interfaces.accounting.' +
                                'Accounting.init_from_excel')
        log.info('Started to initialize')
        log.info('Clearing table "services"')
        session = models.Session()
        session.query(models.Service).delete()
        session.commit()
        log.info('Resetting auto increments')
        models.engine.execute("DELETE FROM sqlite_sequence WHERE name='services';")
        log.info('Loading from Excel file')
        excel = models.ExcelSheet.load(path, sheet, data_range, headers, required)
        log.info('Saving to database')
        for item in excel.data:
            data = {key: item[key] for row, key, info in headers}
            service = models.Service(**data)
            session.add(service)
        session.commit()

    def get_search_criteria(self):
        return (
            'services.eks_id',
            'articles.number', 'articles.status', 'articles.month',
            'articles.list_nr',
            'stores.address1'
        )

    def get_dossier(self):
        session = models.Session()
        return session.\
            query(models.Service).\
            join(models.Stores).\
            outerjoin(models.Article).\
            all()

    def get_filtered_dossier(self, filters):
        session = models.Session()
        q = session.query(models.Service).\
            join(models.Stores).\
            outerjoin(models.Article).\
            outerjoin(models.StockList)
        for attr, value in filters:
            table, column = attr.split('.', 1)
            if table == 'services':
                cls = models.Service
            elif table == 'articles':
                cls = models.Article
            elif table == 'stocklist':
                cls = models.StockList
            elif table == 'stores':
                cls = models.Stores
            else:
                msg = 'Current Mask can\'t connect to the table '
                msg += table
                raise AttributeError(msg)
            q = q.filter(getattr(cls, column).op('regexp')(value))
        return q

    def get_all_article_numbers(self):
        session = models.Session()
        return session.query(models.StockList.number).all()

    def get_filtered_articles(self, filters):
        session = models.Session()
        q = session.query(models.Article).\
            join(models.StockList).\
            join(models.Service).\
            join(models.Stores)
        for attr, value in filters:
            table, column = attr.split('.', 1)
            if table == 'services':
                cls = models.Service
            elif table == 'articles':
                cls = models.Article
            elif table == 'stocklist':
                cls = models.StockList
            elif table == 'stores':
                cls = models.Stores
            else:
                msg = 'Current Mask can\'t connect to the table '
                msg += table
                raise AttributeError(msg)
            q = q.filter(getattr(cls, column).op('regexp')(value))
        return q

    def get_logsys_macro_template(self, template):
        path = 'app/accounting/templates/logsys_macro_{}_template.txt'.format(
            template
        )
        with open(path, 'r') as f:
            content = f.read()
        return content

    def edit_article(self, id_, changes):
        session = models.Session()
        article = session.query(models.Article).filter_by(id=id_).first()
        for attr, value in changes.items():
            attr = attr.split('.')[-1]
            setattr(article, attr, value)
        session.commit()

    def get_filtered_stores(self, filters):
        session = models.Session(expire_on_commit=False)
        q = session.query(models.Stores).\
            outerjoin(models.Service).\
            join(models.Article).\
            join(models.StockList)
        for attr, value in filters:
            table, column = attr.split('.', 1)
            if table == 'services':
                cls = models.Service
            elif table == 'articles':
                cls = models.Article
            elif table == 'stocklist':
                cls = models.StockList
            elif table == 'stores':
                cls = models.Stores
            else:
                msg = 'Current Mask can\'t connect to the table '
                msg += table
                raise AttributeError(msg)
            q = q.filter(getattr(cls, column).op('regexp')(value))
        stores = q.all()
        return stores

    def get_eks_id(self, ser_id):
        session = models.Session()
        return session.query(models.Service.eks_id).\
            filter_by(id=ser_id).one()[0]

    def add_article(self, *values):
        hdr = ['number', 'service_id', 'amount', 'comment',
               'month', 'status', 'list_nr']
        values = {key: value for key, value in zip(hdr, values)}
        article = models.Article(**values)
        session = models.Session()
        session.add(article)
        session.commit()

    def get_total_price(self, filters):
        q = self.get_filtered_articles(filters).all()
        price = 0.0
        for article in q:
            stock_article = article.stock_article
            price += stock_article.price * article.amount

        return price

    def export_sg322(self, data):
        raise NotImplementedError

    def export_as(self, filepath):
        fil = [['articles.status', 'open']]
        stores = self.get_filtered_stores(fil)
        template_path = os.path.join(
            os.getcwd(),
            'app\\accounting\\templates\\export_as.xtm'
        )

        self.export_excel_file(filepath, template_path, stores)
