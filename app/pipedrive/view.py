

import os
import logging as log
from collections import OrderedDict
import datetime

from tkinter import filedialog

import settings
from localisation import _
from .interface import PipeDrive
from base.templates import Dossier
from base.templates.inputs import item
from . import org_format


logger = log.getLogger('app')


class View(Dossier):
    def __init__(self):
        logger.info('Initializing PipeDrive View')
        interface = PipeDrive()
        super().__init__(interface, 'pipedrive')
        self.title('ProjectPluma - ' + _('masks.pipedrive'))
        self.login()

    def init_main(self):
        super().init_main()

        self.f_main.columnconfigure(0, weight=1)

    def init_search(self):
        super().init_search()

        self.f_search.grid(row=1, column=0, sticky='nwe')
        self.f_search.d_criteria.grid_forget()
        self.f_search.w_criteria.grid_forget()
        self.f_search.w_filters.grid_forget()

        self.f_search.columnconfigure(
            1,
            weight=0
        )
        self.f_search.columnconfigure(
            2,
            weight=0
        )

        self.f_search.d_field.grid(
            row=0,
            column=0,
            sticky='we'
        )
        self.f_search.d_search.grid(
            row=0,
            column=2,
            sticky='w'
        )
        self.f_search.w_field.grid(
            row=1,
            column=0,
            sticky='we'
        )
        self.f_search.w_add_filter.grid(
            row=1,
            column=2,
            sticky='w'
        )

    def _format_dossier(self, dossier):
        data = []
        for id_, org in enumerate(dossier):
            org = org_format.get(org, id_)
            data.append(org)
        return data

    def on_exit(self):
        super().on_exit()
        fil = self.f_search.w_field.get()
        settings.write('default_filter', fil, self.name)

    def get_actions(self):
        actions = OrderedDict([
            ('export_organizations', self.export_organizations),
            ('export_update_startdate', self.export_update_startdate),
        ])
        actions.update(super().get_actions())
        return actions

    def get_filters(self):
        return self.f_search.w_field.get()

    def add_filter(self, field=None, criteria=None, pin=False, refilter=False):
        if refilter:
            self.search()

    def search(self):
        if not self.interface.logged_in:
            return
        filter = self.f_search.w_field.get()
        dossier = self.interface.get_filtered_dossier(filter)
        dossier = self._format_dossier(dossier)
        self.f_tree.w_dossier.dump(dossier)

    def login(self, retry=False):
        def on_done(data):
            username = data['pipedrive.username']
            password = data['pipedrive.password']
            success = self.interface.login(username, password)
            if success:
                settings.write('username', username, self.name)
                crit = self.interface.get_search_criteria()
                values = OrderedDict([[c.name, c.id] for c in crit])
                self.f_search.w_field.values(values)
                fil = settings.get('default_filter', self.name)
                for key, val in values.items():
                    if val == fil:
                        value = key
                        break
                else:
                    value = ''
                self.f_search.w_field.var.set(value)
            else:
                self.login(True)

        title = 'ui.retry_login' if retry else 'ui.login'
        self.show_options(
            title=_(title),
            callback=on_done,
            done_text=_('ui.confirm'),
            items=OrderedDict([
                item(
                    key='pipedrive.username',
                    value=settings.get('username', self.name)
                ),
                item(
                    key='pipedrive.password',
                    show='*'
                ),
            ]),
        )

    def export_organizations(self):
        path = filedialog.asksaveasfilename(
            title=_('ui.save_location'),
            initialdir=os.path.expanduser('~/'),
            defaultextension='.xlsx',
            filetypes=[('Excel', '*.xlsx')],
        )
        data = self.get_filtered()

        self.interface.export_organizations(path, data)

    def export_update_startdate(self):
        self.f_search.w_field.set('')
        path = filedialog.asksaveasfilename(
            title=_('ui.save_location'),
            initialdir=os.path.expanduser('~/'),
            defaultextension='.lsm',
            filetypes=[('LogSys Macro', '*.lsm')],
        )
        if not path:
            return
        data = self.get_filtered()
        organizations = []
        for d in data:
            if not d.storage_position.startswith('$'):
                continue
            try:
                date = datetime.date(*map(int, d.final_startdate.split('-')))
            except AttributeError:
                continue
            if date > datetime.date.today():
                continue
            organizations.append(d)

        self.interface.export_update_startdate(path, organizations)
