

import os
from lib.pipedrive import API, models

import settings
from base.interface import Interface
from lib.util import check


class PipeDrive(Interface):
    def __init__(self):
        super().__init__()
        self.api = API()
        self.logged_in = False
        self.dossier = []

    def get_search_criteria(self):
        if not self.logged_in:
            return ()
        q = self.api.query(models.Filter)
        filters = list(q.get_all())
        filters = list(filter(lambda f: f.type == 'org', filters))
        filters.insert(0, models.Filter(id='', name=''))
        return filters

    def get_dossier(self):
        return self.get_filtered_dossier()

    def get_filtered_dossier(self, filter=''):
        q = self.api.query(models.Organization)
        q.filter_by(filter)
        dossier = q.get_all()

        q = self.api.query(models.OrganizationField)
        fields = list(q.get_all())

        self.dossier = []
        for d in dossier:
            for field in fields:
                if not field.options:
                    continue

                attr = d.ALIASES.get(field.key, field.key)
                try:
                    key = getattr(d, attr)
                except AttributeError:
                    continue
                if key in (None, ''):
                    continue
                for option in field.options:
                    if option['id'] == int(key):
                        value = option['label']
                        break
                else:
                    value = None
                setattr(d, attr, value)

            if d.storage_position and d.storage_position.startswith('$'):
                self.dossier.append(d)
        return self.dossier

    def login(self, user, password):
        proxies = settings.get('proxies', 'pipedrive')
        success = self.api.login(user, password, proxies)
        if success:
            self.logged_in = True
            return True
        return False

    def export_organizations(self, path, data):
        if path:
            template_path = os.path.join(
                os.getcwd(),
                'app\\pipedrive\\templates\\export_organizations.xtm'
            )
            for org in data:
                org.opening_by_eo = check.date_or_none(org.opening_by_eo)
                org.start_eo = check.date_or_none(org.start_eo)
            self.export_excel_file(path, template_path, data)

    def export_update_startdate(self, path, organizations):
        macro = []

        template_path = 'app/pipedrive/templates/export_update_startdate.lsm'
        with open(template_path, 'r') as f:
            template = f.read()

        for org in organizations:
            step = template.format(
                customer_id='0' + str(org.logsys_customer_id),
                date=org.final_startdate,
            )
            macro.append(step)

        macro = '\n'.join(macro)

        with open(path, 'w') as f:
            f.write(macro)
