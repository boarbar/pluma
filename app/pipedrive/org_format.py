

from localisation import _


def get(organization, id_):
    return {
        'key': 'organization-' + str(id_),
        'name': organization.name,
        'value': '',
        'nodes': [{
            'key': 'address-' + str(id_),
            'name': _('organization.address'),
            'value': organization.address,
            }, {
            'key': 'address_admin_area_level_1-' + str(id_),
            'name': _('organization.address_admin_area_level_1'),
            'value': organization.address_admin_area_level_1,
            }, {
            'key': 'address_country-' + str(id_),
            'name': _('organization.address_country'),
            'value': organization.address_country,
            }, {
            'key': 'address_formatted_address-' + str(id_),
            'name': _('organization.address_formatted_address'),
            'value': organization.address_formatted_address,
            }, {
            'key': 'address_locality-' + str(id_),
            'name': _('organization.address_locality'),
            'value': organization.address_locality,
            }, {
            'key': 'address_route-' + str(id_),
            'name': _('organization.address_route'),
            'value': organization.address_route,
            }, {
            'key': 'name-' + str(id_),
            'name': _('organization.name'),
            'value': organization.name,
            }, {
            'key': 'owner_name-' + str(id_),
            'name': _('organization.owner_name'),
            'value': organization.owner_name,
            }, {
            'key': 'status_rampup-' + str(id_),
            'name': _('organization.status_rampup'),
            'value': organization.status_rampup,
            }, {
            'key': 'final_startdate-' + str(id_),
            'name': _('organization.final_startdate'),
            'value': organization.final_startdate,
            }, {
            'key': 'opening_by_eo-' + str(id_),
            'name': _('organization.opening_by_eo'),
            'value': organization.opening_by_eo,
            }, {
            'key': 'rsm-' + str(id_),
            'name': _('organization.rsm'),
            'value': organization.rsm,
            }, {
            'key': 'contract_status-' + str(id_),
            'name': _('organization.contract_status'),
            'value': organization.contract_status,
            }, {
            'key': 'storage_position-' + str(id_),
            'name': _('organization.storage_position'),
            'value': organization.storage_position,
            }, {
            'key': 'technician_in_market-' + str(id_),
            'name': _('organization.technician_in_market'),
            'value': organization.technician_in_market,
            }, {
            'key': 'logsys_customer_id-' + str(id_),
            'name': _('organization.logsys_customer_id'),
            'value': organization.logsys_customer_id,
            }, {
            'key': 'process-' + str(id_),
            'name': _('organization.process'),
            'value': organization.process,
            }, {
            'key': 'start_eo-' + str(id_),
            'name': _('organization.start_eo'),
            'value': organization.start_eo,
            }, {
            'key': 'notified_opening_date-' + str(id_),
            'name': _('organization.notified_opening_date'),
            'value': organization.notified_opening_date,
            }, {
            'key': 'ramp_up_quartal-' + str(id_),
            'name': _('organization.ramp_up_quartal'),
            'value': organization.ramp_up_quartal,
            }, {
            'key': 'sap-' + str(id_),
            'name': _('organization.sap'),
            'value': organization.sap,
        }]
    }
