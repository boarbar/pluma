

from collections import OrderedDict
from tkinter import filedialog

import settings
from localisation import _
from base.templates import Dossier
from base.templates.inputs import item
from .interface import Offer
from . import dossier_format


class View(Dossier):
    def __init__(self):
        interface = Offer()
        self.settings = settings.get_object('offer')
        super().__init__(interface, 'offer')
        self.title('ProjectPluma - ' + _('masks.offer'))

    def _format_dossier(self, dossier):
        data = []

        for offer in dossier:
            tmp = dossier_format.get(
                offer,
            )
            data.append(tmp)
        return data

    def get_selected_pos_id(self):
        iid = self.f_tree.w_dossier.focus()
        while iid != '':
            key, pos_id = iid.split('-')
            if key == 'position':
                return pos_id
            iid = self.f_tree.w_dossier.parent(iid)
        return None

    def create_offer(self):
        items = OrderedDict([
            item(
                key='offers.customer_id',
            ),
            item(
                key='offers.your_sign1',
            ),
            item(
                key='offers.delivery_type',
                value='K',
            ),
        ])

        self.show_options(
            title=_('ui.create_offer'),
            callback=self.interface.add_offer,
            items=items,
        )

    def create_position(self):
        def on_finish(data):
            id_ = self.get_selected_id()
            if id_ is None:
                return
            data['positions.offer_id'] = id_
            offer = self.interface.get_offer(id_)
            data['positions.position'] = len(offer.positions)
            self.interface.add_position(data)
        items = OrderedDict([
            item(
                key='positions.article_number',
            ),
            item(
                key='positions.amount',
                value=1,
            ),
            item(
                key='positions.vk_type',
                value='D',
            ),
            item(
                key='positions.description',
            ),
            item(
                key='positions.cost_location',
            ),
        ])

        self.show_options(
            title=_('ui.create_position'),
            callback=on_finish,
            items=items,
        )

    def edit_offer(self):
        id_ = self.get_selected_id()
        if id_ is None:
            return
        offer = self.interface.get_offer(id_)

        items = OrderedDict([
            item(
                key='offers.customer_id',
                value=offer.customer_id,
            ),
            item(
                key='offers.your_sign1',
                value=offer.your_sign1,
            ),
            item(
                key='offers.delivery_type',
                value=offer.delivery_type,
            ),
            item(
                key='offers.status',
                value=offer.status,
            ),
        ])

        self.show_options(
            title=_('ui.edit_offer'),
            callback=lambda data: self.interface.edit_offer(data, id_),
            items=items,
        )

    def edit_position(self):
        id_ = self.get_selected_pos_id()
        if id_ is None:
            title = _('err.title.no_position_selected')
            msg = _('err.msg.no_position_selected')
            self.show_error(title, msg)
            return

        position = self.interface.get_position(id_)

        items = OrderedDict([
            item(
                key='positions.article_number',
                value=position.article_number,
            ),
            item(
                key='positions.amount',
                value=position.amount,
            ),
            item(
                key='positions.vk_type',
                value=position.vk_type,
            ),
            item(
                key='positions.description',
                value=position.description,
            ),
            item(
                key='positions.cost_location',
                value=position.cost_location,
            ),
            item(
                key='positions.position',
                value=position.position,
            )
        ])

        self.show_options(
            title=_('ui.edit_position'),
            callback=lambda data: self.interface.edit_position(data, id_),
            items=items,
        )

    def remove_offer(self):
        id_ = self.get_selected_id()
        if id_ is None:
            return
        self.interface.remove_offer(id_)

    def remove_position(self):
        id_ = self.get_selected_pos_id()
        if id_ is None:
            title = _('err.title.no_position_selected')
            msg = _('err.msg.no_position_selected')
            self.show_error(title, msg)
            return
        else:
            self.interface.remove_position(id_)

    def offer_from_offer(self):
        id_ = self.get_selected_id()
        if id_ is None:
            return
        old_offer = self.interface.get_offer(id_)

        items = OrderedDict([
            item(
                key='offers.customer_id',
                value=old_offer.customer_id,
            ),
            item(
                key='offers.your_sign1',
                value=old_offer.your_sign1,
            ),
            item(
                key='offers.delivery_type',
                value=old_offer.delivery_type,
            ),
            item(
                key='offers.status',
                value='open',
            ),
        ])

        self.show_options(
            title=_('ui.offer_from_offer'),
            callback=self.interface.offer_from_offer,
            items=items,
        )

    def pos_from_pos(self):
        def on_finish(data):
            id_ = self.get_selected_id()
            if id_ is None:
                return
            data.pop('offers.pfp_hint_key')
            self.interface.pos_from_pos(id_, data)

        id_ = self.get_selected_pos_id()
        if id_ is None:
            title = _('err.title.no_position_selected')
            msg = _('err.msg.no_position_selected')
            self.show_error(title, msg)
            return

        old_pos = self.interface.get_position(id_)

        items = OrderedDict([
            item(
                key='offers.pfp_hint_key',
                value=_('offers.pfp_hint_value'),
                editable=False,
            ),
            item(
                key='positions.amount',
                value=old_pos.amount,
            ),
            item(
                key='positions.article_number',
                value=old_pos.article_number,
            ),
            item(
                key='positions.vk_type',
                value=old_pos.vk_type,
            ),
            item(
                key='positions.description',
                value=old_pos.description,
            ),
            item(
                key='positions.cost_location',
                value=old_pos.cost_location,
            ),
            item(
                key='positions.position',
                value=old_pos.position,
            )
        ])

        self.show_options(
            title=_('ui.position_from_position'),
            callback=on_finish,
            items=items,
        )

    def export_macro(self):
        path = filedialog.asksaveasfilename(
            title=_('ui.save_location'),
            defaultextension='.lsm',
            filetypes=[('LogSys Macro', '*.lsm')],
        )
        id_ = self.get_selected_id()
        if id_ is None:
            return
        self.interface.export_macro(id_, path)

    def export_as_commission(self):
        path = filedialog.asksaveasfilename(
            title=_('ui.save_location'),
            defaultextension='.lsm',
            filetypes=[('LogSys Macro', '*.lsm')],
        )
        id_ = self.get_selected_id()
        if id_ is None:
            return
        offer = self.interface.get_offer(id_)
        data = {
            'customer_id': offer.customer_id,
            'delivery_type': offer.delivery_type,
            'your_sign1': offer.your_sign1,
            'positions': []
        }
        for pos in offer.positions:
            article = self.interface.get_article(pos.article_number)
            data['positions'].append({
                'description': pos.description,
                'amount': pos.amount,
                'unit_price': article.price,
                'item_price': article.price * pos.amount,
            })
        self.interface.export_as_commission(data, path)

    def get_actions(self):
        return OrderedDict([
            ['create_offer', self.create_offer],
            ['create_position', self.create_position],
            ['edit_offer', self.edit_offer],
            ['edit_position', self.edit_position],
            ['remove_offer', self.remove_offer],
            ['remove_position', self.remove_position],
            ['offer_from_offer', self.offer_from_offer],
            ['position_from_position', self.pos_from_pos],
            ['export_macro', self.export_macro],
            ['export_as_commission', self.show_not_implemented],
        ])
