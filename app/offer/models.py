

import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship
from sqlalchemy import String, Integer
from sqlalchemy import Column, ForeignKey

import settings
from base import models
from lib.util import logsys_util

db_type = settings.get('db', 'offer')
db_path = settings.get('db_path', 'offer')
engine = sqlalchemy.create_engine(
    db_type + db_path,
    connect_args={'check_same_thread': False},
)


@sqlalchemy.event.listens_for(engine, 'begin')
def do_begin(conn):
    conn.connection.create_function('regexp', 2, logsys_util.match)

Session = sessionmaker(bind=engine)


class Offer(models.Model):
    __tablename__ = 'offers'
    id = Column(
        Integer, primary_key=True, nullable=False,
        unique=True, autoincrement=True,
    )
    customer_id = Column(String, nullable=False)
    your_sign1 = Column(String, nullable=False)
    delivery_type = Column(String, nullable=False)
    status = Column(String, nullable=False)

    positions = relationship('OfferPos', back_populates='offer')


class OfferPos(models.Model):
    __tablename__ = 'positions'
    id = Column(Integer, primary_key=True, nullable=False)
    article_number = Column(String, nullable=False)
    vk_type = Column(String, nullable=False)
    amount = Column(Integer, nullable=False)
    description = Column(String, nullable=False)
    cost_location = Column(String, nullable=False)
    offer_id = Column(Integer, ForeignKey(Offer.id), nullable=False)
    position = Column(Integer, nullable=False)

    offer = relationship('Offer', back_populates='positions')
