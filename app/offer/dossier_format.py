

from localisation import _


def get(offer):
    TAG = {
        'open': 'green',
        'removed': 'red',
    }
    return {
        # offer
        'key': 'offer-' + str(offer.id),
        'name': offer.customer_id,
        'value': offer.your_sign1,
        'tag': TAG[offer.status],
        'nodes': [{
            # offer customer id
            'key': 'customer_id-' + str(offer.id),
            'name': _('offers.customer_id'),
            'value': offer.customer_id,
            }, {
            # offer your sign1
            'key': 'your_sign1-' + str(offer.id),
            'name': _('offers.your_sign1'),
            'value': offer.your_sign1,
            }, {
            # offer delivery type
            'key': 'delivery_type-' + str(offer.id),
            'name': _('offers.delivery_type'),
            'value': offer.delivery_type,
            }, {
            # offer status
            'key': 'status-' + str(offer.id),
            'name': _('offers.status'),
            'value': offer.status,
            }, {
            # positions
            'key': 'positions-' + str(offer.id),
            'name': _('offers.positions'),
            'value': '',
            'nodes': [{
                # position
                'key': 'position-' + str(position.id),
                'name': position.article_number,
                'value': position.description,
                'nodes': [{
                    # position article number
                    'key': 'article_number-' + str(position.id),
                    'name': _('positions.article_number'),
                    'value': position.article_number,
                    }, {
                    # position amount
                    'key': 'amount-' + str(position.id),
                    'name': _('positions.amount'),
                    'value': position.amount,
                    }, {
                    # position vk type
                    'key': 'vk_type-' + str(position.id),
                    'name': _('positions.vk_type'),
                    'value': position.vk_type,
                    }, {
                    # position description
                    'key': 'description-' + str(position.id),
                    'name': _('positions.description'),
                    'value': position.description,
                    }, {
                    # position cost location
                    'key': 'cost_location-' + str(position.id),
                    'name': _('positions.cost_location'),
                    'value': position.cost_location,
                    }, {
                    # position position
                    'key': 'pospos-' + str(position.id),
                    'name': _('positions.position'),
                    'value': position.position,
                }]
            } for position in sorted(
                offer.positions,
                key=lambda pos: pos.position,
            )]
        }]
    }
