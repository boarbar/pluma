

import logging as log
import datetime

from base.interface import Interface
from . import models
from base import models as bmodels


class Offer(Interface):
    def __init__(self):
        super().__init__()
        self.MAP['get_filtered_dossier'] = self.get_filtered_dossier

    def get_search_criteria(self):
        return (
            'offers.customer_id', 'offers.your_sign1', 'offers.delivery_type',
            'offers.status',
            'positions.article_number', 'positions.vk_type',
            'positions.description', 'positions.cost_location',
        )

    def get_dossier(self):
        session = models.Session()
        return session.\
            query(models.Offer).\
            outerjoin(models.OfferPos).\
            all()

    def get_filtered_dossier(self, filters=[]):
        session = models.Session()
        q = session.query(models.Offer).\
            outerjoin(models.OfferPos)
        for attr, value in filters:
            table, column = attr.split('.', 1)
            if table == 'positions':
                cls = models.OfferPos
            elif table == 'offers':
                cls = models.Offer
            else:
                msg = 'Current Mask can\'t connect to the table '
                msg += table
                raise AttributeError(msg)
            q = q.filter(getattr(cls, column).op('regexp')(value))
        return q

    def get_offer(self, id_):
        session = models.Session()
        return session.query(models.Offer).filter_by(id=id_).first()

    def get_position(self, id_):
        session = models.Session()
        return session.query(models.OfferPos).filter_by(id=id_).first()

    def get_logsys_macro_template(self, specifics):
        path = 'app/offer/templates/logsys_macro_{}.txt'
        path = path.format(specifics)
        with open(path, 'r') as f:
            content = f.read()
        return content

    def get_article(self, number):
        session = bmodels.Session()
        return session.query(bmodels.StockList).\
            filter_by(number=number).first()

    def add_object(self, obj):
        session = models.Session()
        session.add(obj)
        session.commit()

    def add_offer(self, data):
        data['status'] = 'open'
        self.offer_from_offer(data)

    def add_position(self, data):
        data = {key.split('.')[-1]: value for key, value in data.items()}
        position = models.OfferPos(**data)
        self.add_object(position)

    def edit_offer(self, data, id_):
        data = {key.split('.')[-1]: value for key, value in data.items()}
        offer = self.get_offer(id_)
        session = models.Session()
        offer = session.query(models.Offer).filter_by(id=id_).first()
        for attr, value in data.items():
            setattr(offer, attr, value)
        session.commit()

    def edit_position(self, data, id_):
        data = {key.split('.')[-1]: value for key, value in data.items()}
        session = models.Session()
        position = session.query(models.OfferPos).filter_by(id=id_).first()
        for attr, value in data.items():
            setattr(position, attr, value)
        session.commit()

    def remove_offer(self, id_):
        data = {'status': 'removed'}
        self.edit_offer(data, id_)

    def remove_position(self, id_):
        session = models.Session()
        position = session.query(models.OfferPos).filter_by(id=id_)
        position.delete()
        session.commit()

    def offer_from_offer(self, data):
        data = {key.split('.')[-1]: value for key, value in data.items()}
        offer = models.Offer(**data)
        self.add_object(offer)

    def pos_from_pos(self, id_, data):
        data = {key.split('.')[-1]: value for key, value in data.items()}
        pos = models.OfferPos(offer_id=id_, **data)
        self.add_object(pos)

    def export_macro(self, id_, path):
        """Export LogSys macro

        Export an offer as macro by the given ID as offer ID

        Arguments:
            id_ {int} -- ID of offer
            path {str} -- destination path
        """

        tpl_offer = self.get_logsys_macro_template('offer')
        tpl_pos = self.get_logsys_macro_template('offerpos')
        offer = self.get_offer(id_)
        positions = sorted(offer.positions, key=lambda pos: pos.position)
        today = datetime.date.today()

        try:
            first_pos = positions[0]
        except IndexError:
            log.warning(
                'Export Macro failed: offer has no positions, '
                'needs at least one'
            )
            return

        macro = tpl_offer.format(
            customer_id=offer.customer_id,
            your_sign1=offer.your_sign1,
            delivery_type=offer.delivery_type,
            article_number=first_pos.article_number,
            amount=first_pos.amount,
            vk_type=first_pos.vk_type,
            description=first_pos.description,
            cost_location=first_pos.cost_location,
            pos_delivery_date=today
        )
        for pos in positions[1:]:
            macro += tpl_pos.format(
                article_number=pos.article_number,
                amount=first_pos.amount,
                vk_type=pos.vk_type,
                description=pos.description,
                cost_location=pos.cost_location,
                pos_delivery_date=today
            )

        with open(path, 'w') as f:
            f.write(macro)

    def export_as_commission(self, data, path):
        template = self.get_logsys_macro_template('commission')
        for pos in data['positions']:
            position = template.format(
                customer_id='',
                your_sign1='',
                delivery_type='',
                article_number='',
                amount='',
                vk_type='',
                description='',
                cost_location='',
            )

