#!usr/bin/python
"""Project Pluma - Automation tool between different forms of data.

This project is meant to automate a lot of manual user tasks.
It should entierly replace the need to edit Excel files.

####################
### - Features - ###
####################
- Import of existing Excel files
- Export of Excel files in certain formats
- Fast and extensive Regex-like search
- Clean display of data
- Shell
- Extensible apps


#####################
### - Structure - ###
#####################
User specific settings can be specified in user_settings.yml in the main folder,
but also in every app folder.
This is a very modular system, so everything important is put together in main.py.
main.py sets up the logging process, looks for updates and starts the app specified in the settings.
The app has all it's relevant modules in app/<app>.

This program follows the MVC (Model-View-Controller) Pattern.
The Models are located in base/models or app/<app>/models
and makes extensive use of SQLAlchemy and OpenPyxl.
The View is located in the app/<app>/view.py files (each app requires one).
The Controller is located in the app/<app>/interface.py files (each app should have one).
"""


import os
import sys
import time
import logging as log
import subprocess
import yaml

import settings
from app import create_app


logger = log.getLogger('app')
logger.setLevel(log.INFO)

file_name = 'log/{}.log'.format(time.strftime('%Y%m%d'))
handler = log.FileHandler(file_name)
handler.setLevel(log.INFO)

formatter = log.Formatter('%(asctime)s - %(name)s - ' +
                          '%(levelname)s: %(message)s',
                          datefmt='%d.%m.%Y %I:%M:%S %p')
handler.setFormatter(formatter)

logger.addHandler(handler)


log_files = os.listdir('log')
for name in log_files:
    if name.endswith('.log'):
        continue
    log_files.remove('.gitignore')

if len(log_files) > 10:
    log_files = sorted(log_files, reverse=True)
    log_files = log_files[10:]
    for f in log_files:
        os.remove('log/' + f)

sett = settings.get_object()
to_update = sett.get('auto_update', True)

if to_update:
    process = subprocess.Popen(
        [
            os.path.join(os.getcwd(), 'lib\\git\\bin\\git.exe'),
            'pull',
            'https://boarbar@bitbucket.org/boarbar/pluma.git',
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    out, err = process.communicate()
    with open('update.yml', 'w') as f:
        content = yaml.dump({
            'content': str(out),
            'error': str(err),
        })
        f.write(content)

if 'test' in sys.argv:
    try:
        import test
        test.run()
    except Exception:
        logger.error('Can\'t load test')
else:
    try:
        app = create_app(settings.get('mask'))
        app.mainloop()
    except Exception:
        print('error')
        logger.error('', exc_info=True)
