

import logging as log

import sqlalchemy
from sqlalchemy.orm import relationship
from sqlalchemy import String, Integer, Float, DateTime
from sqlalchemy import Column, ForeignKey

import settings
from lib.util import logsys_util
from . import Model

engine = sqlalchemy.create_engine(
    settings.get('db'),
    connect_args={'check_same_thread': False},
)
conn = engine.connect()


@sqlalchemy.event.listens_for(engine, 'begin')
def do_begin(conn):
    conn.connection.create_function('regexp', 2, logsys_util.match)


class Service(Model):
    __tablename__ = 'services'
    id = Column(Integer, primary_key=True, nullable=False)
    ifd_nr = Column(Integer)
    eks_id = Column(Integer, ForeignKey('stores.id'))
    process = Column(String)
    status = Column(String)
    debit_date = Column(DateTime)
    actual_date = Column(DateTime)
    sys_nr = Column(String)
    orderer = Column(String)

    articles = relationship('Article', back_populates='service', lazy='joined')
    store = relationship('Stores', back_populates='services', lazy='joined')

    def __repr__(self):
        r = (
            'id={}'.format(self.id),
            'ifd_nr={}'.format(self.ifd_nr),
            'eks_id={}'.format(self.eks_id, nullable=False),
            'process={}'.format(self.process, nullable=False),
            'status={}'.format(self.status, nullable=False),
            'debit_date={}'.format(self.debit_date),
            'actual_date={}'.format(self.actual_date),
            'sys_nr={}'.format(self.sys_nr),
            'orderer={}'.format(self.orderer),
            'articles={}'.format(self.articles),
            'store={}'.format(self.store)
        )
        return '<Service(' + ', '.join(r) + ')>'


class StockList(Model):
    __tablename__ = 'stocklist'
    number = Column(String, primary_key=True)
    description = Column(String)
    price = Column(Float)
    comment = Column(String)
    type1 = Column(Integer)
    type2 = Column(Integer)
    type3 = Column(Integer)
    type4 = Column(Integer)
    refit_honeywell = Column(Integer)
    refit_telenot = Column(Integer)
    min_stored = Column(Integer)
    deliverer = Column(String)

    articles = relationship('Article', back_populates='stock_article')
    orders = relationship('Order', back_populates='stock_article')


class Stores(Model):
    __tablename__ = 'stores'
    id = Column(Integer, primary_key=True, nullable=False)
    object_nr = Column(Integer, nullable=False)
    project = Column(String, nullable=False)
    object_status = Column(String, nullable=False)
    system_status = Column(String, nullable=False)
    hint = Column(String)
    product_type = Column(String, nullable=False)
    constructor1 = Column(String, nullable=False)
    constructor2 = Column(String)
    inst_date = Column(String)
    warranty_end = Column(String)
    deinst_date = Column(String)
    tel = Column(String)
    tel2 = Column(String)
    mobile = Column(String)
    fax = Column(String)
    email = Column(String)
    address1 = Column(String, nullable=False)
    address2 = Column(String)
    address3 = Column(String)
    street = Column(String)
    zip = Column(String)
    city = Column(String)
    customer_info1 = Column(String)
    rl = Column(String)
    gl = Column(String)
    room_plan = Column(String)
    nsl = Column(String)
    nsl_id = Column(String)

    services = relationship('Service', back_populates='store',
                            lazy='joined')


class Storage(Model):
    __tablename__ = 'storage'
    id = Column(Integer, primary_key=True, nullable=False)
    location = Column(String, nullable=False)
    storage = Column(String, nullable=False)
    place = Column(String, nullable=False)
    allocation = Column(String)
    number = Column(String, nullable=False)
    prod_desc = Column(String)
    desc = Column(String)
    serial = Column(String)
    amount = Column(Integer, nullable=False)
    price = Column(String, nullable=False)
    holdback = Column(String)
    expiry_date = Column(String)
    swap_nr = Column(String)
    job_nr = Column(String)
    pos = Column(String)
