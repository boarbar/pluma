

import sqlalchemy
from sqlalchemy import String, Integer
from sqlalchemy import Column

from . import Model


engine = sqlalchemy.create_engine(
    'mysql+pypyodbc://DWWOL1'
)
conn = engine.connect()


class Kunden(Model):
    __tablename__ = 'Kunden'
    object_nr = Column(Integer, nullable=False)
    project = Column(String, nullable=False)
    object_status = Column(String, nullable=False)
    system_status = Column(String, nullable=False)
    hint = Column('Bemerkung', String)
    product_type = Column(String, nullable=False)
    constructor1 = Column(String, nullable=False)
    constructor2 = Column(String)
    inst_date = Column(String)
    warranty_end = Column(String)
    deinst_date = Column(String)
    tel = Column('Tel', String)
    tel2 = Column(String)
    mobile = Column(String)
    fax = Column('Fax', String)
    email = Column('EMail', String)
    address1 = Column(String, nullable=False)
    address2 = Column(String)
    address3 = Column(String)
    street = Column(String)
    zip = Column(String)
    city = Column(String)
    customer_info1 = Column('KundBez1', String)
    rl = Column(String)
    gl = Column(String)
    room_plan = Column(String)
    nsl = Column(String)
    nsl_id = Column(String)
