

import logging as log

import sqlalchemy
from sqlalchemy.orm import relationship
from sqlalchemy import String, Integer, Boolean
from sqlalchemy import Column, ForeignKey, Table
import openpyxl as pyx

from . import Model, Session


class Article(Model):
    __tablename__ = 'articles'
    id = Column(Integer, primary_key=True)
    number = Column(String, ForeignKey('stocklist.number'),
                    nullable=False)
    service_id = Column(Integer, ForeignKey('services.id'),
                        nullable=False)
    amount = Column(Integer)
    comment = Column(String)
    month = Column(Integer)
    status = Column(String)
    list_nr = Column(Integer)

    stock_article = relationship('StockList',
                                 back_populates='articles')
    service = relationship('Service', back_populates='articles',
                           lazy='joined')

    def __repr__(self):
        r = (
            'id={}'.format(self.id),
            'number={}'.format(self.number),
            'service_id={}'.format(self.service_id),
            'amount={}'.format(self.amount),
            'comment={}'.format(self.comment),
            'month={}'.format(self.month),
            'status={}'.format(self.status),
            'list_nr={}'.format(self.list_nr),
            'stock_article={}'.format(self.stock_article),
            'service={}'.format(self.service)
        )
        return '<Article(' + ', '.join(r) + ')>'


class Order(Model):
    __tablename__ = 'orders'
    id = Column(Integer, primary_key=True, nullable=False)
    number = Column(String, ForeignKey('stocklist.number'), nullable=False)
    amount = Column(Integer, nullable=False)
    status = Column(String, nullable=False)
    month = Column(Integer, nullable=False)
    year = Column(Integer, nullable=False)

    stock_article = relationship(
        'StockList', back_populates='orders', lazy='joined'
    )


class Excel:
    def __init__(self, path, sheet, data_range, headers, model, required=[0]):
        self.model = model

        self.data = []
        wb = pyx.load_workbook(path, data_only=True, read_only=True)

        sheet = wb[sheet]
        for row in sheet.iter_rows(data_range):
            if row[0].value is None:
                break
            required_exist = [row[i].value is not None for i in required]
            if not all(required_exist):
                values = [r.value for r in row if r.value is not None]
                log.warning('Could not load row: ' + str(values))
                continue
            data = {
                key: row[irow].value for irow, key in headers.items()
            }
            model = self.model(**data)
            self.data.append(model)

    def save(self):
        session = Session()
        for model in self.data:
            session.add(model)
        try:
            session.commit()
        except sqlalchemy.exc.IntegrityError as e:
            session.rollback()
            raise e


class ExcelSheet:
    def __init__(self, headers, path='', sheet='sheet1', *data):
        self.headers = headers
        self.data = data

    def letters_to_number(letters):
        number = 0
        for letter in letters:
            if letter.isalpha():
                number = number * 26 + (ord(letter.upper()) - ord('A')) + 1
        return number

    def load(path, sheet_name, data_range, headers, required=()):
        """Load an Excel document as Model

        Arguments:
            path {str} -- full or relative path of the file
            data_range {str} -- area that contains data; ex: "A1:D123"
            headers {list} -- column/s, key, header name;
                              ex: [('A', 'id', 'ID'),
                                   ('B:C', 'names', 'Names')]
            required {str/None} -- stop iteration if row contains no value for
                                   these attributes (default: {()})
        """

        wb = pyx.load_workbook(path, data_only=True, read_only=True)
        sheet = wb[sheet_name]
        data = []
        for row in sheet.iter_rows(data_range):
            item = {}
            for rang, attr, name in headers:
                i_range = ExcelSheet.letters_to_number(rang)-1
                val = row[i_range].value
                if attr in required and val is None:
                    break
                else:
                    item[attr] = val
            else:
                data.append(item)
                continue
            break
        return ExcelSheet(headers, path, sheet_name, *data)

    def _save(self, file, path, sheet_name, write_headers=False):
        if sheet_name is None:
            try:
                sheet = file.get_active_sheet()
            except IndexError:
                sheet = file.create_sheet()
        else:
            sheet = file[sheet_name]

        if write_headers:
            for column, key, name in self.headers:
                cell = sheet.cell('{col}{row}'.format(col=column, row=1))
                cell.value = name

        for row, item in enumerate(self.data, 1 + int(write_headers)):
            for column, key, name in self.headers:
                cell = sheet.cell('{col}{row}'.format(col=column, row=row))
                cell.value = item[key]

        file.save(path)

    def save(self):
        file = pyx.load_workbook(self.path)
        self._save(file, self.path, self.sheet)

    def save_as(self, path, sheet=None, write_headers=False):
        file = pyx.Workbook()
        self._save(file, path, sheet, write_headers)

    def save_with_template(
        self, path, template_path, sheet=None, write_headers=False
    ):
        template = pyx.load_workbook(template_path)
        self._save(template, path, sheet, write_headers)


class Macro:
    def __init__(self):
        self.steps = []

    def add_step(self, key, value):
        self.steps.append((key, value))

    def export_logsys(self, filepath):
        with open(filepath, 'w+') as f:
            for key, value in self.steps:
                if key == 'FIELD':
                    f.write(' '.join([key, value]))
                else:
                    f.write(value)
