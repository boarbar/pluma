

import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import settings
from lib.util import logsys_util

db_type = settings.get('db')
db_path = settings.get('db_path')
engine = sqlalchemy.create_engine(
    db_type + db_path,
    connect_args={'check_same_thread': False},
)
conn = engine.connect()


@sqlalchemy.event.listens_for(engine, 'begin')
def do_begin(conn):
    conn.connection.create_function('regexp', 2, logsys_util.match)

Session = sessionmaker(bind=engine)
Model = declarative_base()


from .common import Article, Order
from .common import Excel, ExcelSheet, Macro
from .read_only import Service, StockList, Stores, Storage
