

import app


class API:
    """API for Pluma

    This API should only be exposed as single object.
    It only provides wrapper functions to common actions on the GUI and the
    underlying interface.
    """

    def search(self):
        """Initiate search on GUI

        Takes arguments from the field and criteria fields.
        You can set them with set_search_field and set_search_criteria
        respectively.
        """
        app.app.add_filter()

    def execute_action(self):
        """Execute selected Action

        Executes the action specified in the action selection.
        """
        app.app.make_action()

    def set_search_field(self, value):
        """Set Field value for Search

        Sets the value for the field, which corresponds roughly to the
        database table name.

        Keyword Arguments:
            value -- value to set
        """
        app.app.f_search.w_field.var.set(value)

    def set_search_criteria(self, value):
        """Set Criteria value for Search

        Sets the value for the criteria, which is evaluated as a modified
        RegEx. Enable help (F1) and hover over the criteria field to see
        the modifications.

        Keyword Arguments:
            value -- value to set
        """
        app.app.f_search.w_criteria.var.set(value)

    def set_action(self, value):
        """Set Action

        Sets the action which is to be executed. The value needs to be a
        valid action from the selection.

        Keyword Arguments:
            value -- value to set
        """
        app.app.f_action.w_type.var.set(value)

    def get_search_field(self):
        """Get Field value for Search

        Returns:
            value
        """
        return app.app.f_search.w_field.var.get()

    def get_search_criteria(self):
        """Get Criteria value for Search

        Returns:
            value
        """
        return app.app.f_search.w_criteria.var.get()

    def get_action(self):
        """Get Action

        Returns the name of the selected action.

        Returns:
            value
        """
        return app.app.f_action.w_type.var.get()

    def pythons(self):
        """Pythons (Snakes)

        Opens or closes an easteregg game,
        which is largely inspired by "Snakes".
        The title is a play on the snake species
        and programming language "Python".
        """
        app.app.toggle_pythons()

    def sweeper(self):
        """HolyHandgrenadeSweeper (MineSweeper)

        Opens or closes an easteregg game,
        which is largely inspired by "MineSweeper".
        The title is a play on the Holy Hand Grenade of Antioch,
        used against the Rabbit of Caerbannog in
        Monty Python and the Holy Grail.
        """
        app.app.toggle_sweeper()
