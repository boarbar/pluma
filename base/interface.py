

import os
import logging
import code
import locale

import openpyxl as pyx

from lib.util import maths
import settings
import localisation

log = logging.getLogger('app')


class Interface:
    def __init__(self):
        log.info('Initializing Interface')
        self.MAP = {
            'get_search_criteria': self.get_search_criteria,
            'get_dossier': self.get_dossier,
            'get_filtered_dossier': self.get_filtered_dossier,
        }
        self.settings = {}
        localisation.set_language(settings.get('lan'))

    def __call__(self, cmd, *args, **kwargs):
        try:
            cmd = self.MAP[cmd]
        except KeyError:
            raise NotImplementedError
        return cmd(*args, **kwargs)

    def get_search_criteria(self):
        return ()

    def get_dossier(self, filters):
        return None

    def get_filtered_dossier(self):
        return None

    def export_excel_file(self, path, template_path, data):
        log.info('Started to export Excel file')

        def get_file():
            if template:
                log.info('Getting file from template')
                file = pyx.load_workbook(template)
            else:
                log.info('Getting file without template')
                file = pyx.Workbook()
            return file

        def get_sheet():
            if sheet_name is None:
                try:
                    sheet = file.get_active_sheet()
                except IndexError:
                    log.info('Create new sheet')
                    sheet = file.create_sheet()
                else:
                    log.info('Get active sheet')
            else:
                log.info('Get specified sheet')
                sheet = file[sheet_name]
            return sheet

        def write_in_cell(sheet, i, j, value):
            if not j.isalpha():
                j = maths.number_to_letters(j)
            cell = sheet.cell('{col}{row}'.format(col=j, row=str(i)))
            cell.value = value

        def write_data(sheet, data, headers, start_n, start_l):
            for i, obj in enumerate(data, start_n):
                interpreter.locals['obj'] = obj
                for j, header in enumerate(headers, start_l):
                    try:
                        interpreter.runcode('_ = ' + header)
                    except TypeError:
                        value = None
                    else:
                        value = interpreter.locals['_']
                    write_in_cell(sheet, i, str(j), value)

        def write_manual(sheet, data):
            for cell, value in data.items():
                cell = sheet.cell(cell)
                interpreter.runcode('_ = ' + value)
                cell.value = interpreter.locals['_']

        log.info('Opening template code file')
        with open(template_path, 'r') as f:
            content = f.read()

        interpreter = code.InteractiveInterpreter({})
        log.info('Running template code')
        interpreter.runcode(content)

        log.info('Setting required attributes')
        try:
            data_range_start = interpreter.locals['data_range_start']
            data_sources = interpreter.locals['data_sources']
        except KeyError as e:
            raise SyntaxError(
                'XTM file requires the attribute {},'
                ' but could not find it'.format(str(e))
            )

        log.info('Setting optional attributes')
        sheet_name = interpreter.locals.get('sheet_name', None)
        template = interpreter.locals.get('template', None)
        data_iterable = interpreter.locals.get('data_iterable', None)
        manual_data_distribution = interpreter.locals.get(
            'manual_data_distribution', None)
        export_file_name = interpreter.locals.get('export_file_name', None)

        start = maths.split_numbers_and_letters(data_range_start)
        data_start_l = maths.letters_to_number(start[1])
        data_start_n = int(start[0])
        log.debug('Starting column: ' + str(data_start_l))
        log.debug('Starting row   : ' + str(data_start_n))

        if data_iterable:
            log.info('Creating multiple files')
            for origin in data:
                interpreter.locals['ori'] = origin
                file = get_file()
                interpreter.runcode('_ = ' + data_iterable)
                objects = interpreter.locals['_']
                sheet = get_sheet()
                write_data(
                    sheet, objects, data_sources, data_start_n, data_start_l
                )
                if manual_data_distribution:
                    write_manual(sheet, manual_data_distribution)
                interpreter.runcode('_ = ' + export_file_name)
                file_name = interpreter.locals['_']
                save_path = os.path.join(path, file_name)
                file.save(save_path)
        else:
            log.info('Creating file')
            interpreter.locals['ori'] = data
            file = get_file()
            sheet = get_sheet()
            write_data(sheet, data, data_sources, data_start_n, data_start_l)
            if manual_data_distribution:
                write_manual(manual_data_distribution)
            file.save(path)
        log.info('Process complete')

    def format_currency(self, number):
        locale.setlocale(locale.LC_ALL, 'DE')
        return locale.currency(number, grouping=True)
