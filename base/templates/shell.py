

import code
import inspect
import threading

from tkinter import Toplevel, filedialog
from tkinter.scrolledtext import ScrolledText

from base.api import API
from user import functions


class Shell(Toplevel):
    LC = '>>> '

    def __init__(self):
        super().__init__()
        self.title('Shell')
        self.iconbitmap('icon.ico')

        self.entry = ScrolledText(self, bg='#000', fg='#fff',
                                  insertbackground='#fff')
        self.entry.bind('<Return>', lambda ev: self.execute())
        self.entry.bind('<BackSpace>', lambda ev: self.remove())
        self.entry.bind('<Control-Up>', lambda ev: self.cycle_history())
        self.entry.bind('<Control-Down>', lambda ev: self.cycle_history(1))
        self.entry.bind('<Key>', self.on_key_press)

        self.entry.grid(row=0, column=0, sticky='nswe')
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

        self.interpreter = code.InteractiveConsole(
            {
                'print': self.add_line,
                'help': lambda item: self.add_line(inspect.getdoc(item)),
                'this': API(),
                'functions': functions,
                'load': self.execute_file,
            },
            filename='<string>',
        )
        self.interpreter.write = self.add_line
        self.history = []
        self.history_pos = 0

        self.entry.focus()

        self.add_newline()

        self.protected = self.entry.index('end-1c')  # end index of protection

    def on_key_press(self, ev):
        # Return and Backspace aren't executed
        if ev.keysym in ('Up', 'Down', 'Left', 'Right'):
            return
        i1, i2 = self.entry.index('insert').split('.')
        p1, p2 = self.protected.split('.')
        if int(i1) < int(p1) or int(i2) < int(p2):
            return 'break'

    def simulate_event(self, sequence):
        w = self.focus_get()
        w.event_generate(sequence)

    def execute_file(self):
        def _execute():
            path = filedialog.askopenfilename()
            if not path:
                return
            with open(path, 'r') as f:
                code = f.read()
            self.interpreter.push(code)
        threading.Thread(target=_execute).start()

    def digest(self, macro_list):
        for args in macro_list:
            self.entry.insert('end', '::'.join(args))
            self.execute()

    def add_line(self, *args, sep=' ', end='\n'):
        text = sep.join(map(str, args))
        self.entry.insert('end', text + end)
        self.entry.mark_set('insert', 'end')
        self.entry.see('insert')

    def cycle_history(self, step=-1):
        if not self.history:
            return 'break'
        i = self.history_pos + step
        l = len(self.history)
        if i < 0:
            i = 0
        elif i >= l:
            i = l - 1
        self.history_pos = i
        self.entry.delete('end-1c linestart+4c', 'end')
        self.entry.insert('end', self.history[i])
        return 'break'

    def execute(self):
        cmd = self.entry.get('end-1c linestart+4c', 'end-1c')
        if cmd == 'exit()':
            self.destroy()
            return'break'
        try:
            incomplete = self.interpreter.push(cmd + '\n')
        except KeyError as e:
            self.add_line(str(e))
        else:
            self.history.append(cmd)
            self.history_pos += 1
            if incomplete:
                self.LC = '... '
            else:
                self.LC = '>>> '
        self.add_newline()
        return 'break'

    def remove(self):
        row, column = self.entry.index('insert').split('.')
        prow, pcolumn = self.protected.split('.')
        if int(row) >= int(prow) and int(column) > int(pcolumn):
            self.entry.delete('insert-1c')
        return 'break'

    def add_newline(self):
        if self.entry.index('end-1c').split('.')[1] == '0':
            nl = ''
        else:
            nl = '\n'
        self.entry.insert('end', nl + self.LC)
        self.protected = self.entry.index('end-1c')

    def clear(self):
        self.entry.delete('1.0', 'end')
