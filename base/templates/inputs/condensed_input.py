

from collections import OrderedDict

from tkinter import Button as TButton, StringVar, messagebox, filedialog
from tkinter.ttk import Frame, Button, Label, Entry

from localisation import _
from lib.custom.widgets.autobox import AutoBox


class CondensedInput(Frame):
    def __init__(self, parent, title='', info=''):
        super().__init__(parent, borderwidth=5, relief='ridge',
                         width=500, height=500)

        self.items = {}

        self.w_title = Label(self, text=title)
        self.w_close = TButton(
            self,
            text='X',
            width=2,
            relief='groove',
            command=lambda: self.callback(None)
        )
        self.w_info = Label(self, text=info)
        self.w_field = AutoBox(self)
        self.w_field.bind(
            '<Button-1>',
            lambda ev: self._save_current_val)
        self.w_field.bind(
            '<<ComboboxSelected>>',
            lambda ev: self.change_to(self.get_current_key()))
        var = StringVar()
        self.w_value = Entry(self, textvariable=var)
        self.w_value.var = var
        self._reinit_w_value()
        self.w_prev = Button(
            self, text=_('ui.previous'),
            command=lambda: self.change_to_relative(1))
        self.w_next = Button(
            self, text=_('ui.next'),
            command=lambda: self.change_to_relative(-1))
        self.w_save = Button(
            self, text=_('ui.save'),
            command=self.save)

        self._grid()

    def _reinit_w_value(self):
        self.w_value.focus()
        self.w_value.bind(
            '<Return>',
            lambda ev: self.change_to_relative(1))
        self.w_value.bind(
            '<FocusOut>',
            lambda ev: self._save_current_val)

    def _grid(self):
        self.w_title.grid(row=0, column=0, sticky='we')
        self.w_close.grid(row=0, column=1, sticky='e')
        self.w_info.grid(row=1, column=0, columnspan=2, sticky='we')
        self.w_field.grid(row=2, column=0, columnspan=2, sticky='we')
        self.w_value.grid(row=3, column=0, columnspan=2, sticky='nswe')
        self.w_prev.grid(row=4, column=0, sticky='we')
        self.w_next.grid(row=4, column=1, sticky='we')
        self.w_save.grid(row=5, column=0, columnspan=2, sticky='we')

    def grid(self, **kwargs):
        self._grid()
        super().grid(**kwargs)

    def pack(self, **kwargs):
        self._grid()
        super().pack(**kwargs)

    def place(self, **kwargs):
        self._grid()
        super().place(**kwargs)

    def _save_current_val(self):
        """Save currently displayed value.

        Makes a check for validity first.
        """
        key = self.get_current_key()
        if key is not None:
            item = self.items[key]
            value = self.w_value.var.get()
            if item['value'] is None and value == '':
                value = None
            if not item['validity'](value) or value is False:
                messagebox.showerror(
                    title=_('err.title.value_not_valid'),
                    message=_('err.msg.value_not_valid', val=item['val_msg'])
                )
                return False
            self.items[key]['value'] = value
            return True
        else:
            return False

    def _widget_factory(self, key):
        def label(value, *args):
            var = StringVar(value=value)
            widget = Label(self, textvariable=var)
            widget.var = var
            widget.get = widget.var.get
            return widget

        def entry(value, *args):
            var = StringVar(value=value)
            widget = Entry(self, textvariable=var)
            widget.var = var
            widget.get = widget.var.get
            return widget

        def combo(value, values, auto_take, *args):
            var = StringVar(value=value)
            values = OrderedDict([v for v in values.items()])
            widget = AutoBox(self, values=values, textvariable=var,
                             auto_take=auto_take)
            widget.var = var
            return widget

        def filepath_chooser(*args):
            def set_entry():
                path = filedialog.askdirectory()
                widget.var.set(path)
                widget.entry.focus()
            var = StringVar()
            widget = Frame(self)
            widget.entry = Entry(widget, textvariable=var)
            widget.button = Button(
                master=widget,
                text='...',
                command=set_entry
            )
            widget.entry.grid(row=0, column=0)
            widget.button.grid(row=0, column=1)
            widget.var = var
            widget.get = widget.var.get
            widget.bind = widget.entry.bind
            widget.focus = widget.button.focus
            return widget

        val = self.items[key]
        if val['special'] == 'filepath_chooser':
            widget = filepath_chooser()
        elif val['values']:
            widget = combo(val['value'], val['values'], val['auto_take'])
        elif val['editable']:
            widget = entry(val['value'])
        else:
            widget = label(val['value'])
        if val['default']:
            widget.var.set(val['default'])
        return widget

    def populate(self, title, callback, items, info=''):
        """Populate widgets and set variables.

        Args:
            title: title to display
            callback: function to call when saving
            items: dictionary of keys as displayed values and
                    values as export values
        """
        self.w_title['text'] = title
        self.w_info['text'] = info

        self.callback = callback
        self.items = items

        self.w_field.values(
            OrderedDict([[_(k), k] for k in items.keys()]))

        key = self.get_check()
        if key is None:
            key = list(items.keys())[0]
        self.change_to(key)

        self.w_value.focus()

    def get_current_key(self):
        return self.w_field.get()

    def get_check(self, key=None, step=1):
        """Return the key that is closest to key and is empty.

        Keyword Arguments:
            key {text} -- starting key (default: {None})
            step {number} -- relative next key (default: {1})

        Returns:
            text or None -- None if all fields are filled
        """
        keys = list(self.items.keys())
        if key is None:
            index = 0
        else:
            index = keys.index(key)
        keys = keys[index::step] + keys[:index:step]
        for rkey in keys:
            val = self.items[rkey]
            if key == rkey:
                continue
            elif val['value'] is None:
                return rkey
        return None

    def change_to(self, key):
        """Change display of key and value."""
        values = self.w_field.values()
        values = {v: k for k, v in values.items()}
        self.w_field.var.set(values[key])

        self.w_value.grid_remove()
        self.w_value = self._widget_factory(key)
        self._reinit_w_value()
        self.w_value.grid(row=3, column=0, columnspan=2, sticky='nswe')

    def change_to_relative(self, step):
        """Changes to the index relative of the current index."""
        key = self.get_current_key()
        key = self.get_check(key, step)
        if key is None:
            self.save()
        else:
            valid = self._save_current_val()
            if valid:
                self.change_to(key)

    def save(self):
        """Call callback with the stored values."""
        self._save_current_val()
        self.callback(self.items)
