

from .detail_input import DetailInput
from .condensed_input import CondensedInput


def item(key, value=None, values=None, default=None, validity=None,
         editable=True, special=None, val_msg='', auto_take=False, show=None):
    if validity is None:
        def validity(w): return w != ''
    return [
        key, {
            'value': value,
            'values': values,
            'default': default,
            'validity': validity,
            'editable': editable,
            'special': special,
            'val_msg': val_msg,
            'auto_take': auto_take,
            'show': show,
            }]
