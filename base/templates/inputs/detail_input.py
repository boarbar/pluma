

from collections import OrderedDict

from tkinter import StringVar, Button as TButton, filedialog
from tkinter.ttk import Frame, Label, Entry, Button

from localisation import _
from lib.custom.widgets.autobox import AutoBox


class DetailInput(Frame):
    def __init__(self, parent, title='', info=''):
        super().__init__(parent, borderwidth=5, relief='ridge')

        self.w_title = Label(self, text=title)
        self.w_close = TButton(
            self,
            text='X',
            width=2,
            relief='groove',
            command=lambda: self.callback(None)
        )
        self.w_save = Button(self, text=_('ui.save'), command=self.save)
        self.widgets = OrderedDict()

        self._grid()

    def _grid(self):
        self.w_title.grid(row=0, column=0, columnspan=2)
        self.w_close.grid(row=0, column=1, sticky='e')
        row = 1
        for widget in self.widgets.values():
            widget['value'].bind(
                '<Return>',
                lambda ev: self.save(),
            )
            widget['label'].grid(row=row, column=0, sticky='w')
            widget['value'].grid(row=row, column=1, sticky='we')
            row += 1
        self.w_save.grid(row=row, column=0, columnspan=2)

    def _widget_factory(self, key):
        def label(value, *args):
            var = StringVar(value=value)
            widget = Label(self, textvariable=var)
            widget.var = var
            widget.get = widget.var.get
            return widget

        def entry(value, show, *args):
            var = StringVar(value=value)
            widget = Entry(self, textvariable=var, show=show)
            widget.var = var
            widget.get = widget.var.get
            return widget

        def combo(value, values, auto_take, *args):
            var = StringVar(value=value)
            values = OrderedDict([v for v in values.items()])
            widget = AutoBox(self, values=values, textvariable=var,
                             auto_take=auto_take)
            widget.var = var
            return widget

        def filepath_chooser(*args):
            def set_entry():
                path = filedialog.askdirectory()
                widget.var.set(path)
                widget.entry.focus()
            var = StringVar()
            widget = Frame(self)
            widget.entry = Entry(widget, textvariable=var)
            widget.button = Button(
                master=widget,
                text='...',
                command=set_entry
            )
            widget.entry.grid(row=0, column=0)
            widget.button.grid(row=0, column=1)
            widget.var = var
            widget.get = widget.var.get
            widget.bind = widget.entry.bind
            widget.focus = widget.button.focus
            return widget

        def file_chooser(*args):
            def set_entry():
                path = filedialog.askopenfilename()
                widget.var.set(path)
                widget.entry.focus()
            var = StringVar()
            widget = Frame(self)
            widget.entry = Entry(widget, textvariable=var)
            widget.button = Button(
                master=widget,
                text='...',
                command=set_entry
            )
            widget.entry.grid(row=0, column=0)
            widget.button.grid(row=0, column=1)
            widget.var = var
            widget.get = widget.var.get
            widget.bind = widget.entry.bind
            widget.focus = widget.button.focus
            return widget

        val = self.items[key]
        if val['special'] == 'filepath_chooser':
            widget = filepath_chooser()
        elif val['special'] == 'file_chooser':
            widget = file_chooser()
        elif val['values']:
            widget = combo(val['value'], val['values'], val['auto_take'])
        elif val['editable']:
            widget = entry(val['value'], val['show'])
        else:
            widget = label(val['value'])
        if val['default']:
            widget.var.set(val['default'])
        return widget

    def grid(self, *args, **kwargs):
        self._grid()
        super().grid(*args, **kwargs)

    def populate(self, title, callback, items, done_text=_('ui.save'),
                 selected=None):

        for widget in self.widgets.values():
            widget['label'].grid_forget()
            widget['value'].grid_forget()
        self.widgets.clear()

        self.w_title['text'] = title
        self.w_save['text'] = done_text
        self.callback = callback
        self.items = items

        for key, options in items.items():
            self.widgets[key] = {
                'label': Label(self, text=_(key)),
                'value': self._widget_factory(key),
            }
        self._grid()
        if not selected:
            selected = list(items.keys())[0]
        self.widgets[selected]['value'].focus()

    def save(self):
        data = {}
        for key, widget in self.widgets.items():
            item = self.items[key]
            value = widget['value'].var.get()
            valid = item['validity']
            if not valid(value):
                widget['value'].focus()
                return

            values = item['values']
            if values:
                value = values[value]
            data[key] = value
        self.callback(data)
