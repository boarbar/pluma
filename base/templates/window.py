

import logging as log

from tkinter import Tk, messagebox

import settings
from localisation import _


class Window(Tk):
    def __init__(self, **kwargs):
        log.info('Initializing Window ' + str(type(self)))
        super().__init__(**kwargs)
        self.title('ProjectPluma')
        self.iconbitmap('icon.ico')
        self.protocol('WM_DELETE_WINDOW', self.on_exit)

    def on_exit(self):
        if settings.get('message_on_close'):
            result = messagebox.askquestion(
                _('ask.title.really_quit'),
                _('ask.msg.really_quit')
            )
            if result == 'no':
                return
        self.destroy()

    def close(self):
        self.on_exit()
