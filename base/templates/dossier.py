

import os
import logging as log
from collections import OrderedDict

import yaml
from tkinter import filedialog
from tkinter import Menu as TMenu, StringVar, messagebox, TclError
from tkinter.ttk import Frame, Menubutton, Button, Label, Entry
from tkinter.ttk import Scrollbar, Separator

import settings
from localisation import _, languages as LANGUAGES, set_language
from . import Window, Shell
from .inputs import DetailInput
import app as APP
from base.templates.inputs import item
from lib.custom.widgets.change_tree import ChangeTree
from lib.custom.widgets.autobox import AutoBox
from lib.custom.widgets.tooltip import ToolTip
from lib.custom.widgets.items_listing import ItemsListing
from lib.custom.widgets.statusbar import Statusbar
from lib.custom.widgets.pythons import Pythons
from lib.custom.widgets.holy_handgrenade_sweeper import HolyHandgrenadeSweeper


class Dossier(Window):
    def __init__(self, interface, name, **kwargs):
        super().__init__(**kwargs)
        self.name = name
        self.interface = interface
        self.tooltips_enabled = False
        self.tooltips = []
        self.MAP = {
            'toggle_tooltips': self.toggle_tooltips,
            'select': self.select_field,
            'search': lambda: self.add_filter(refilter=True),
            'insert': lambda value: self.focus_get().var.set(value),
        }

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        self.init_main()
        self.init_status()
        self.init_menu()
        self.init_search()
        self.init_tree()
        self.init_actions()
        self.init_options()

        self.bind_all('<F1>', lambda ev: self.toggle_tooltips())
        self.bind_all('<F2>', lambda ev: self.f_search.w_criteria.focus())
        self.bind_all('<F3>', lambda ev: self.f_action.w_type.focus())
        self.bind_all('<F4>', lambda ev: self.f_tree.w_dossier.focus_set())

        self.check_update()

    def init_main(self):
        log.info('Initializing main frame.')
        self.f_main = Frame(self)
        self.s_search_actions = Separator(self.f_main, orient='vertical')

        self.f_main.rowconfigure(2, weight=1)
        self.f_main.columnconfigure(2, weight=1)

        self.f_main.grid(
            row=0,
            column=0,
            sticky='nswe',
            padx=10,
            pady=10
        )
        self.s_search_actions.grid(
            row=1,
            column=1,
            sticky='nswe',
            padx=10,
        )

    def init_menu(self):
        log.info('Initializing menu frame.')
        self.f_menu = Frame(self.f_main)

        self.f_menu.w_shell = Menubutton(
            self.f_menu,
            text=_('ui.shell')
        )
        self.tooltips.append(ToolTip(
            self.f_menu.w_shell,
            _('ui.tooltip.shell'),
            False,
        ))
        self.f_menu.w_shell_menu = TMenu(
            self.f_menu.w_shell
        )
        self.f_menu.w_shell.configure(
            menu=self.f_menu.w_shell_menu
        )
        self.f_menu.w_shell_menu.add_command(
            label=_('ui.shell'),
            command=lambda: Shell()
        )
        self.f_menu.w_shell_menu.add_command(
            label=_('ui.execute_code'),
            command=lambda: Shell().execute_file()
        )

        self.f_menu.w_masks = Menubutton(
            self.f_menu,
            text=_('ui.masks')
        )
        self.tooltips.append(ToolTip(
            self.f_menu.w_masks,
            _('ui.tooltip.masks'),
            False,
        ))
        self.f_menu.w_masks_menu = TMenu(
            self.f_menu.w_masks
        )
        self.f_menu.w_masks.configure(
            menu=self.f_menu.w_masks_menu
        )
        for value in sorted(APP.APPS.keys()):
            self.f_menu.w_masks_menu.add_command(
                label=_('masks.' + value),
                command=lambda value=value: self.change_to(value)
            )

        def save_settings(data):
            for key, value in data.items():
                module, key = key.split('.')[-2:]
                if module == 'main':
                    module = None
                settings.write(key, value, module)
                set_language(data['settings.main.lan'])

        self.f_menu.w_settings = Button(
            self.f_menu,
            text=_('ui.settings'),
            command=lambda: self.show_options(
                title=_('ui.settings'),
                callback=save_settings,
                items=self._get_settings(),
            )
        )

        self.f_menu.w_info = Button(
            self.f_menu,
            text='?',
            width=3
        )
        self.f_menu.t_info = ToolTip(
            self.f_menu.w_info,
            _('ui.tooltip.general_help')
        )

        self.f_menu.columnconfigure(1, weight=1)
        self.f_menu.columnconfigure(2, weight=1)

        self.f_menu.grid(row=0, column=0, sticky='nwe', columnspan=3)
        self.f_menu.w_shell.grid(row=0, column=0, sticky='nw')
        self.f_menu.w_masks.grid(row=0, column=1, sticky='nw')
        self.f_menu.w_settings.grid(row=0, column=2, sticky='nw')
        self.f_menu.w_info.grid(row=0, column=5, sticky='ne')

    def init_search(self):
        log.info('Initializing search frame.')
        self.f_search = Frame(self.f_main)

        self.f_search.d_field = Label(
            self.f_search,
            text=_('ui.field')
        )
        self.f_search.d_criteria = Label(
            self.f_search,
            text=_('ui.filter')
        )
        self.f_search.d_search = Label(
            self.f_search,
            text=_('ui.search')
        )
        self.f_search.w_field = AutoBox(
            self.f_search
        )
        self.tooltips.append(ToolTip(
            self.f_search.w_field,
            _('ui.tooltip.search_field'),
            False
        ))
        var = StringVar()
        self.f_search.w_criteria = Entry(
            self.f_search,
            textvariable=var
        )
        self.tooltips.append(ToolTip(
            self.f_search.w_criteria,
            _('ui.tooltip.search_criteria'),
            False
        ))
        self.f_search.w_criteria.var = var
        self.f_search.w_add_filter = Button(
            self.f_search,
            command=lambda: self.add_filter(refilter=True),
            text='+', width=4
        )
        self.tooltips.append(ToolTip(
            self.f_search.w_add_filter,
            _('ui.tooltip.filter'),
            False
        ))
        self.f_search.w_filters = ItemsListing(
            self.f_search
        )
        self.tooltips.append(ToolTip(
            self.f_search.w_filters,
            _('ui.tooltip.filters'),
            False
        ))

        self.f_search.columnconfigure(
            0,
            weight=1
        )
        self.f_search.columnconfigure(
            1,
            weight=1
        )
        self.f_search.columnconfigure(
            2,
            weight=1
        )

        self.f_search.grid(row=1, column=0, sticky='nw')
        self.f_search.d_criteria.grid(
            row=0,
            column=1,
            sticky='we'
        )
        self.f_search.d_field.grid(
            row=0,
            column=0,
            sticky='we'
        )
        self.f_search.d_search.grid(
            row=0,
            column=2,
            sticky='we'
        )
        self.f_search.w_criteria.grid(
            row=1,
            column=1,
            sticky='we'
        )
        self.f_search.w_field.grid(
            row=1,
            column=0,
            sticky='we'
        )
        self.f_search.w_add_filter.grid(
            row=1,
            column=2,
            sticky='we'
        )
        self.f_search.w_filters.grid(
            row=2,
            column=0,
            columnspan=3,
            sticky='we',
        )

        self.f_search.w_criteria.focus()

        def _add_filter():
            self.add_filter(refilter=True)
            self.f_action.w_type.focus()
        self.f_search.w_criteria.bind(
            '<Return>',
            lambda ev: _add_filter()
        )
        self.f_search.w_criteria.bind(
            '<Escape>',
            lambda ev: self.f_search.w_filters.clear()
        )
        self.f_search.w_field.bind(
            '<Return>',
            lambda ev: _add_filter()
        )
        self.f_search.w_field.bind(
            '<Escape>',
            lambda ev: self.f_search.w_criteria.focus()
        )
        self.f_search.w_filters.on_pop = lambda key: self.search()
        self.f_search.w_filters.on_clear = self.search
        self.f_search.w_filters.on_toggle = self.search

        exp = self.interface('get_search_criteria')
        crt = _(exp)
        values = OrderedDict([val for val in zip(crt, exp)])
        self.f_search.w_field.values(values)

        filters = settings.get('default_filters', self.name)
        for field, criteria in filters.items():
            key = '::'.join((field, criteria))
            value = ' = '.join((_(field), criteria))
            self.f_search.w_filters.add(key, value)
            self.f_search.w_filters.pin(key)

    def init_actions(self):
        log.info('Initializing actions frame.')
        self.f_action = Frame(self.f_main)

        self.f_action.d_type = Label(
            self.f_action,
            text=_('ui.action')
        )

        self.f_action.w_type = AutoBox(self.f_action)
        self.tooltips.append(ToolTip(
            self.f_action.w_type,
            _('ui.tooltip.action_type'),
            False
        ))
        self.f_action.w_execute = Button(
            self.f_action,
            command=self.make_action,
            text='\u2794',
            width=4,
        )
        self.f_action.w_selection = AutoBox(self.f_action)
        self.f_action.w_options = AutoBox(self.f_action)

        self.f_action.columnconfigure(0, weight=1)

        self.f_action.grid(row=1, column=2, sticky='nw')
        self.f_action.d_type.grid(row=0, column=0, sticky='we')
        self.f_action.w_type.grid(row=1, column=0, sticky='we')
        self.f_action.w_execute.grid(row=1, column=1, sticky='we')

        self.f_action.w_type.bind(
            '<Escape>',
            lambda ev: self.f_search.w_criteria.focus()
        )

        exp = list(self.get_actions().keys())
        act = _(exp)
        values = OrderedDict([val for val in zip(act, exp)])
        self.f_action.w_type.values(values)
        self.f_action.w_type.bind(
            '<Return>',
            lambda ev: self.make_action())

    def init_tree(self):
        log.info('Initializing tree frame.')
        self.f_tree = Frame(self.f_main)
        self.f_tree.w_dossier = ChangeTree(self.f_tree)
        self.f_tree.w_dossier.column(0, stretch=False, width=170)
        self.f_tree.w_dossier.column(1, stretch=True, minwidth=5000)

        ysb = Scrollbar(
            self.f_tree,
            orient='vertical',
            command=self.f_tree.w_dossier.yview
        )
        xsb = Scrollbar(
            self.f_tree,
            orient='horizontal',
            command=self.f_tree.w_dossier.xview
        )
        self.f_tree.w_dossier.configure(
            yscroll=ysb.set,
            xscroll=xsb.set
        )

        self.f_tree.w_pythons = Pythons(self, self.f_tree)
        self.f_tree.w_hhsweeper = HolyHandgrenadeSweeper(self, self.f_tree)

        self.f_tree.columnconfigure(1, weight=1)
        self.f_tree.rowconfigure(0, weight=1)

        self.f_tree.grid(row=2, column=0, sticky='nswe', columnspan=3)
        self.f_tree.w_dossier.grid(
            row=0,
            column=0,
            sticky='nswe',
            columnspan=2
            )
        ysb.grid(row=0, column=2, sticky='ns')
        xsb.grid(row=1, column=0, sticky='we', columnspan=2)

        self.search()

    def init_status(self):
        self.f_status = Statusbar(self.f_main)
        self.f_status.grid(row=3, column=0, sticky='nwe', columnspan=3)

    def init_options(self):
        self.f_options = Frame(self)

        self.f_options.w_options = DetailInput(self.f_options)

    def on_exit(self):
        super().on_exit()
        pinned = {}
        for pin in self.f_search.w_filters.pinned:
            key, value = pin.split('::')
            pinned[key] = value
        settings.write('default_filters', pinned, self.name)
        settings.write('mask', self.name)

    def check_update(self):
        auto_update = settings.get('auto_update', self.name)
        if not auto_update:
            return
        with open('update.yml', 'r') as f:
            content = yaml.load(f.read())

        if 'abort' in content['error']:
            title = _('err.title.update_failed')
            msg = _('err.msg.update_failed', err=content['error'])
            self.show_error(title, msg)

    def _get_settings(self):
        return OrderedDict([
            item(
                key='settings.main.lan',
                default=_('lan.' + settings.get('lan')),
                values={
                    _('lan.' + key): key for key in LANGUAGES.keys()
                },
            ),
            item(
                key=''.join(('settings.', self.name, '.db_path')),
                default=settings.get('db_path', self.name),
                special='file_chooser',
            ),
        ])

    def _format_dossier(self, dossier):
        return []

    def show_not_implemented(self):
        title = _('info.title.not_implemented')
        msg = _('info.msg.not_implemented')
        messagebox.showwarning(title, msg)

    def get_actions(self):
        return OrderedDict([
            ('export_excel_file', self.export_excel_file),
        ])

    def get_selected_id(self):
        iid = self.f_tree.w_dossier.get_topmost_focused()
        if iid is '':
            return None
        else:
            return int(iid.split('-')[-1])

    def select_field(self, field):
        {
            'search_criteria': self.f_search.w_criteria,
        }[field].focus()

    def toggle_tooltips(self):
        for tooltip in self.tooltips:
            if self.tooltips_enabled:
                tooltip.enabled = False
            else:
                tooltip.enabled = True
        self.tooltips_enabled = not self.tooltips_enabled

    def add_filter(self, field=None, criteria=None, pin=False, refilter=False):
        if field is None:
            field = self.f_search.w_field.get()
        if criteria is None:
            criteria = self.f_search.w_criteria.get()
        key = '::'.join((field, criteria))
        value = ' = '.join((_(field), criteria))
        self.f_search.w_filters.add(key, value)
        if pin is True:
            self.f_search.w_filters.pin(key)

        if refilter:
            self.search()

    def make_action(self):
        action = self.f_action.w_type.get()
        actions = self.get_actions()
        actions[action]()

    def get_filters(self):
        filters = self.f_search.w_filters.items.keys()
        filters = [fil.split('::', 1) for fil in filters]
        return filters

    def get_filtered(self):
        filters = self.get_filters()
        if filters:
            dossier = self.interface('get_filtered_dossier', filters)
        else:
            dossier = self.interface('get_dossier')
        return dossier

    def search(self):
        log.info('Started Filtering')
        dossier = self.get_filtered()
        dossier = self._format_dossier(dossier)
        self.f_tree.w_dossier.dump(dossier)
        log.info('Filtering Done')

    def show_options(self, **kwargs):
        def callback(data):
            self.f_options.grid_remove()
            self.f_options.w_options.grid_remove()
            set_states(self.f_main, state='enable')
            if data:
                real_callback(data)
            self.search()
            self.f_search.w_criteria.focus()

        def set_states(widget, state='disabled'):
            try:
                widget.configure(state=state)
            except TclError:
                pass
            for child in widget.winfo_children():
                set_states(child, state=state)

        self.f_options.grid(
            row=0,
            column=0,
            padx=10,
            pady=10
        )
        self.f_tree.w_dossier.select_first()
        set_states(self.f_main)
        self.f_options.w_options.grid()
        real_callback = kwargs['callback']
        kwargs['callback'] = callback
        self.f_options.w_options.populate(**kwargs)

    def change_to(self, app):
        self.destroy()
        APP.create_app(app)

    def ask_question(self, title, msg):
        return messagebox.askquestion(title, msg)

    def show_error(self, title, msg):
        messagebox.showerror(title, msg)

    def toggle_pythons(self):
        pythons = self.f_tree.w_pythons
        run = pythons.run
        if run:
            pythons.grid_forget()
        else:
            pythons.grid(row=0, column=0, sticky='nswe', columnspan=2)

    def toggle_sweeper(self):
        sweeper = self.f_tree.w_hhsweeper
        run = sweeper.run
        if run:
            sweeper.grid_forget()
        else:
            sweeper.grid(row=0, column=0, sticky='nswe', columnspan=2)
            sweeper.setup()

    def export_excel_file(self):
        template_path = filedialog.askopenfilename(
            initialdir=os.path.join(
                os.getcwd(), 'app', self.name, 'templates'
            ),
            filetypes=[('eXcel TeMplate script', '*.xtm')],
        )
        if not template_path:
            return

        path = filedialog.asksaveasfilename(
            title=_('ui.save_location'),
            defaultextension='.xlsx',
            filetypes=[('Excel', '*.xlsx')],
        )
        if not template_path:
            return

        data = self.get_filtered()

        self.interface.export_excel_file(path, template_path, data)
